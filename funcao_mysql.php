<?php

	$usuario   = "root";
	$senha     = "";
    $nome_banco= "3info1_prfc";


	try{

		$conexao = new PDO("mysql:host=localhost;", $usuario, $senha);
		$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


	
        
		$sql10= "USE $nome_banco;";


		$conexao->exec($sql10);
		$sql10 = "CREATE FUNCTION apagar_avaliacao(id_profissional int, id_leitor int) RETURNS varchar(100) CHARSET latin1
					BEGIN
					declare tipo_avaliacao int;
					declare positivo int;
					declare negativo int;
					declare mensagem varchar(100);

					SELECT avaliacao INTO tipo_avaliacao from avaliacao_profissional_usuario where cod_usuario_avaliado = id_profissional and cod_usuario_avaliando=id_leitor;

					SELECT avaliacao_positiva into positivo  FROM profissional WHERE cod_usuario=id_profissional;
					SELECT avaliacao_negativa into negativo  FROM profissional WHERE cod_usuario=id_profissional;
					IF tipo_avaliacao = 1 then UPDATE profissional SET avaliacao_positiva = positivo - 1 where cod_usuario=id_profissional;
					ELSEIF tipo_avaliacao=0 then UPDATE profissional SET avaliacao_negativa= negativo - 1 where cod_usuario=id_profissional;
					END IF;
					DELETE FROM avaliacao_profissional_usuario WHERE cod_usuario_avaliando = id_leitor and cod_usuario_avaliado=id_profissional;
										
					SET mensagem = 'a avaliacao foi exluida!';
					RETURN mensagem;
				END
			";
		$conexao->exec($sql10);


        
        
	}catch(PDOException $erro){

		echo "Erro: " . $erro->getMessage();

	}

