        <div class="ui stackable large menu">
          <a href="{menul1}" class="item">
           <i class="home large icon "></i>
          </a> 
          <a href="{menul2}" class="item">
            Artigos
          </a>
          <a href="{menul3}" class="item">
           Profissionais
          </a>
          <a href="{menul4}" class="item">
            Mensagens
          </a>
          <a href="{menul5}" class="item">
            Minhas Curtidas
          </a>
          <div class="right container menu">
            <div class="item">
              <form action="Controllers/processa_pesquisa.php" method="post">      
                <div class="ui transparent icon input">
                  <input placeholder="Pesquisar..." type="text" name="pesquisa">
                  <i class="search icon"></i>
                </div>
              </form>
            </div>
              <div class="ui simple large dropdown item">
                Minha Conta
                <i class="dropdown icon"></i>
                  <div class="menu">
                    <a href='{menul6}' class="item">Atualizar Dados</a>
                    <a href='{menul7}' class="item">Excluir Conta</a>
                  </div>
              </div>
                <a href="{menul8}" class="item">
                <i class="sign out large icon" title="Sair"></i>
                </a>
          </div>
        </div>
