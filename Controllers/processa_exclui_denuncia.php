<?php
include_once "../Models/Administrador.php"; 
include_once "../Models/Usuario.php";
include_once "../Models/Comentario.php";

	$codigo_comentario = $_POST['cod_comentario'];
	$codigo_denunciado = $_POST['cod_denunciado'];
	$status_usuario = 1;
	$status_denuncia = 0;


$adm = new Administrador();
$dados = $adm->cadastrar_status_denuncia($status_denuncia,$codigo_comentario);

$user = new Usuario();
$status_user = $user->altera_status_usuario($status_usuario, $codigo_denunciado);

$denuncia_apagar = new Administrador();
$apagar = $denuncia_apagar->apagar_denuncias($codigo_comentario);

$denunciado = new Comentario();
$denunciado_coment = $denunciado->bloqueia_comentario_adm( "NULL", $codigo_comentario);


header('location:../?pgs=modal_exclui_denuncia_adm');