<?php
include_once "../Models/Profissional.php";
include_once "../Models/Artigo.php";



$pesquisa = $_POST['pesquisa'];


if(isset($pesquisa) && !empty($pesquisa)){
	$profissional = new Profissional();
	$nome_profissional = $profissional->pesquisa_profissionais($pesquisa);
	$artigo = new Artigo();
	$busca = $artigo->pesquisa_artigo($pesquisa);
	
	session_start();
	
	$pesquisa = array('profissionais' => $nome_profissional ,
					  'artigos' => $busca );
	session_start();
	$_SESSION['pesquisa'] = $pesquisa;

	//$url= '&pesquisa='.urlencode(serialize($pesquisa));
	header('location:../?pgs=pesquisa');

}else{ 
	header('location:../?pgs=modal_pesquisa');
}

?>