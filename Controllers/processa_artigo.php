<?php

require "../Models/Artigo.php";
require "../Models/Profissional.php";

$titulo = $_POST['titulo'];
$data = date('y/m/d');
$texto = $_POST['text_artigo'];
$tags = $_POST['tags'];
$codigo = $_POST['codigo_usuario'];


$foto_artigo = $_FILES['fotoartigo'];

if (empty($foto_artigo)) {
	$foto_artigo['name'] = "artigo_padrao.png";
}else{

	$foto_upload = new Profissional();
	$foto_artigo = $foto_upload->upload_imagem($foto_artigo);
}

$artigo = new Artigo();
$artigo->cadastrar_artigo($data, $titulo, $foto_artigo, $texto, $tags, $codigo);

header("location:../?pgs=modal_cadastro_artigo");