<?php
include_once '../Models/Usuario.php';
include_once '../Models/Profissional.php';
include_once '../Models/Login.php';
include_once '../Models/Logout.php';


$login = new Login();
$ta_logado = $login->taLogado();
$tipo = $login->retorna_tipo();


if ($ta_logado == true) {
	$codigo_usuario = $login->retorna_codigo_usuario();
}else{
	echo "Usuario não autenticado";
}


$usuario = new Usuario();
$usuario->altera_status_usuario(0, $codigo_usuario);

$logout = new Logout();
$logout->logout();
header("location:../?pgs=inicial");