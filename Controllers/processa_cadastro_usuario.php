<?php

include_once "../Models/Usuario.php";
include_once "../Models/Profissional.php";
include_once "../Models/Login.php"; 
include_once "../Models/Database.php"; 


$email = $_POST['email_usuario'];
$user = new Usuario();
$dados = $user->valida_dados($_POST);

	 $conexao = Database::getConnection();

        $sql = "SELECT email_usuario FROM usuario WHERE email_usuario = '$email';";
        $busca = $conexao->query($sql);
        $consulta = $busca->fetch(PDO::FETCH_ASSOC);
        if(isset($consulta['email_usuario'])){
        	$erro = 'email já cadastrado';
        	header("location:../?pgs=form_cadastro_usuario&erro=".$erro);
        	} else{

				if (is_array($dados)) {
					
					$nome = $dados['nome_usuario'];
					$email = $dados['email_usuario'];
					$senha= password_hash( $dados['senha_usuario'], PASSWORD_DEFAULT);
					$confsenha = password_verify($dados['confsenha_usuario'], $senha);
					$rec_pergunta = $dados['rec_senha_pergunta'];
					$rec_resposta = $dados['rec_senha_resposta'];

					if ($dados['tipo_user'] == "Leitor") {
						$tipo_usuario = 3;

							if ($senha == $confsenha) {
								$usuario = new Usuario(); 
								$usuario->cadastrar_usuario($dados['nome_usuario'], $email, $senha, $tipo_usuario, $rec_pergunta, $rec_resposta);
								
								$login = new Login();
								$novo_login = $login->efetua_login($email, $_POST['senha_usuario']);
								header("location:../?pgs=modal_cadastro_usuario");
								unset($_SESSION['dados_cadastro']);
							}else{
								session_start();
								$_SESSION['dados_cadastro'] = $dados;
								header("location:../?pgs=modal_erro_confirmacao_cadastro");
							}
						}else {
							echo 'não há tipo de usuario';
						}

						
					if ($dados['tipo_user'] == "Profissional") {
						
						$tipo_usuario = 2;

						if ($senha == $confsenha) {

							$usuario = new Usuario(); 
							$id_usuario = $usuario->cadastrar_usuario($nome, $email, $senha, $tipo_usuario, $rec_pergunta, $rec_resposta);

							$login = new Login();
							$novo_login = $login->efetua_login($email, $_POST['senha_usuario']);
							unset($_SESSION['dados_cadastro']);
							header('location:../?pgs=cadastro_profissional');
						}else{
								session_start();
								$_SESSION['dados_cadastro'] = $_POST;
								header("location:../?pgs=modal_erro_confirmacao_cadastro");
							}	
					}
				}else{
					session_start();
					$_SESSION['dados_cadastro'] = $_POST;
					header("location:../?pgs=form_cadastro_usuario&erro=".$dados);
					}

	}