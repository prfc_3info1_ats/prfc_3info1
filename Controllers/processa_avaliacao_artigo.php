<?php
require '../Models/Artigo.php';
require '../Models/Login.php';

#1 é negativo, 0 é positivo
$usuario = new Login();
$cod_usuario = $usuario->retorna_codigo_usuario();

$id_artigo = $_POST['id'];

$conexao = Database::getConnection();
$sql = "SELECT cod_av_artigo from avaliacao_artigo where cod_usuario = $cod_usuario and cod_artigo = $id_artigo;";
$busca = $conexao->query($sql);
$usuario_avaliando = $busca->fetch(PDO::FETCH_ASSOC);

if(empty($usuario_avaliando['cod_av_artigo']) and is_null($usuario_avaliando['cod_av_artigo'])){
	$avaliacao = $_POST['avaliacao'];
	if ($avaliacao == "sim") {
		$cadastra_avaliacao_banco = 1;
	} elseif ($avaliacao == "não") {
		$cadastra_avaliacao_banco = 0;
	}	

	$artigo = new Artigo();
	$artigo->cadastra_avaliacao_artigo($id_artigo, $cod_usuario, $cadastra_avaliacao_banco);

	header('location:../?pgs=mostra_artigo&id='.$_POST['id']);

} else{
	header('location:../?pgs=modal_excluir_avaliacao_artigo&id='.$id_artigo);
	
}


