<?php
include_once '../Models/Artigo.php';
include_once '../Models/Login.php';


$usuario_logado = new Login();
$cod_usuario = $usuario_logado->retorna_codigo_usuario();

if (empty($_POST['id_artigo'])) {
	$id_artigo=$_GET['id_artigo'];
	header('location:../?pgs=minhas_curtidas&pg=1');

}else{
	$id_artigo = $_POST['id_artigo'];
	header('location:../?pgs=mostra_artigo&id='.$id_artigo);

}

$artigo = new Artigo();
$artigo->apagar_avaliacao($cod_usuario, $id_artigo);