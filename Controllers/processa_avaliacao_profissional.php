<?php
include_once '../Models/Profissional.php';
include_once '../Models/Login.php';


$usuario = new Login();
$cod_usuario = $usuario->retorna_codigo_usuario();
$id_profissional = $_POST['id']; 
$conexao = Database::getConnection();
$select = "SELECT cod_usuario_avaliando FROM avaliacao_profissional_usuario WHERE cod_usuario_avaliado = $id_profissional;";
$busca = $conexao->query($select);
$usuario_avaliando = $busca->fetchAll(PDO::FETCH_ASSOC);


$avaliacao = $_POST['avaliacao_profissional'];
if ($avaliacao == "sim") {
	$cadastra_avaliacao_banco = 1;
} elseif ($avaliacao == "não") {
	$cadastra_avaliacao_banco = 0;
}	
if (!empty($usuario_avaliando)) {
	foreach ($usuario_avaliando as $codigo) {
		if (in_array($cod_usuario, $codigo, true) ) {
			$avaliou = true;
		} else{
			$avaliou = false;
		}
	}
}else{
	$avaliou = false;
}
if ($avaliou == true) {
	header('location:../?pgs=modal_excluir_avaliacao_profissional&id='.$id_profissional);
}else{
	$artigo = new Profissional($id_profissional);
	$artigo->cadastra_avaliacao_profissional($id_profissional, $cod_usuario, $cadastra_avaliacao_banco);
}

