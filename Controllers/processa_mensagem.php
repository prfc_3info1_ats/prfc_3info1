<?php

require "../Models/Mensagem.php";

$texto_mensagem = $_POST['texto_mensagem'];
$data = date('y/m/d');

if (!empty($texto_mensagem)) {
$codigo_remetente = $_POST['codigo_usuario_remetente'];
$codigo_destinatario = $_POST['codigo_usuario_destinatario'];

$mensagem = new Mensagem();
$cadastro_mensagem = $mensagem->cadastrar_mensagem($codigo_remetente, $codigo_destinatario, $texto_mensagem, $data);

header("location:../?pgs=mensagem&id=$codigo_destinatario");

}else{
	$codigo_destinatario = $_POST['codigo_usuario_destinatario'];
	header("location:../?pgs=modal_mensagem_vazia&id=$codigo_destinatario");
}