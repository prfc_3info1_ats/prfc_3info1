<?php
require '../Models/Comentario.php';
require '../Models/Login.php';
require '../Models/Usuario.php';

$usuario = new Login();
$cod_usuario = $usuario->retorna_codigo_usuario();
$data_comentario = date('y/m/d');

$id_artigo = $_POST['id_artigo'];
$texto_comentario = $_POST['texto_comentario'];


if (!empty( $_POST['id_comentario'])) {
	$id_comentario = $_POST['id_comentario'];
	$comentario = new Comentario();
	$comentario->cadastra_resposta($texto_comentario, $data_comentario,  $id_artigo, $cod_usuario, $id_comentario);

	header("location:../?pgs=mostra_artigo&id=$id_artigo");

}else{
	$comentario = new Comentario($texto_comentario);
	$comentario->cadastra_comentario($texto_comentario, $data_comentario,  $id_artigo, $cod_usuario);

	header("location:../?pgs=mostra_artigo&id=$id_artigo");

}



