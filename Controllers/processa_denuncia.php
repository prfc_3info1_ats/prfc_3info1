<?php  
include_once '../Models/Comentario.php';
include_once '../Models/Administrador.php';

$cod_comentario = $_GET['codigo_coment'];
$cod_denunciador = $_GET['codigo_denunciador'];
$cod_artigo = $_GET['codigo_artigo'];
$tipo_comentario = $_GET['tipo_comentario'];


if ($tipo_comentario == "comentario_puro") {
	$denunciado = new Comentario();
	$cod = $denunciado->busca_apenas_comentario_denuncia($cod_comentario);
	$cod_denunciado = $cod[0]['cod_usuario'];


}else{
	$denunciado = new Comentario();
	$cod = $denunciado->busca_resposta_comentario_denuncia($cod_comentario);
	$cod_denunciado = $cod[0]['cod_usuario'];
}

$denuncia_busca = new Administrador();
$busca_denuncia = $denuncia_busca->busca_denuncia($cod_comentario, $cod_denunciado, $cod_denunciador);

if (!empty($busca_denuncia)) {
	header('location:../?pgs=modal_denuncia_ja_realizada&id='.$cod_artigo);
	exit();
}

else{

include_once '../Models/Usuario.php';
$usuario = new Usuario();
$cadastra = $usuario->cadastra_denuncia( $cod_denunciado, $cod_denunciador,$cod_artigo, $cod_comentario );

header("location:../?pgs=modal_denuncia_concluida&id=".$cod_artigo);

}
