<?php 
include_once "../Models/Usuario.php";
include_once "../Models/Profissional.php";
include_once "../Models/Login.php";


$logado = new Login();
$codigo = $logado->retorna_codigo_usuario();
$tipo = $logado->retorna_tipo();


if ($tipo == 3) {
	if ( strlen($_POST['senha_usuario']) <=12) {
		$user = new Usuario();
		$dados = $user->valida_dados($_POST);
		$_POST['senha_usuario'] = password_hash( $_POST['senha_usuario'], PASSWORD_DEFAULT);
		$_POST['confsenha_usuario'] = $_POST['senha_usuario'];
	}elseif ($_POST['senha_usuario'] == $_POST['confsenha_usuario'] && strlen($_POST['senha_usuario']) >12){
		$user = new Usuario();
		$informacoes = array('nome_usuario' => $_POST['nome_usuario'],
		 						'email_usuario' => $_POST['email_usuario'],
		 						'rec_senha' => $_POST['rec_senha'],
		 						'rec_senha_resposta'=> $_POST['rec_senha_resposta']
		 						);
		$dados = $user->valida_dados($informacoes);

	}
	

	if (is_array($dados)) {
		$usuario = new Usuario(); 
		$usuario->atualiza_cadastro($_POST, $codigo);
		header("location:../?pgs=modal_atualiza_dados");
	}else{
		header("location:../?pgs=altera_dados_usuario&erro=".$dados);
	}

}elseif ($tipo == 2) {
	if ( $_POST['senha_usuario'] == $_POST['confsenha_usuario'] && strlen($_POST['senha_usuario']) >12) {
		$dados_padrao = array('nome_usuario' => $_POST['nome_usuario'],
		 						'email_usuario' => $_POST['email_usuario'],
		 						'rec_senha' => $_POST['rec_senha'],
		 						'rec_senha_resposta'=> $_POST['rec_senha_resposta']
		 						);
	}else{
		 $dados_padrao = array('nome_usuario' => $_POST['nome_usuario'],
		 						'email_usuario' => $_POST['email_usuario'],
		 						'senha_usuario' => $_POST['senha_usuario'],
		 						'confsenha_usuario' => $_POST['confsenha_usuario'],
		 						'rec_senha' => $_POST['rec_senha'],
		 						'rec_senha_resposta'=> $_POST['rec_senha_resposta']
		 						);

		}
	$user = new Usuario();
	$dados_usuario = $user->valida_dados($dados_padrao);

	$tel = $_POST['tel'];
	$cpf =  $_POST['cpf'];
	$data= $_POST['data'];
	$profissional = new Profissional();
	$dados_profissional = $profissional->valida_dados_profissional($tel, $cpf, $data);

if (is_array($dados_usuario) and $dados_profissional == false) {
	if ($_POST['senha_usuario'] == $_POST['confsenha_usuario']) {

		$_POST['senha_usuario'] = password_hash( $_POST['senha_usuario'], PASSWORD_DEFAULT);
		$_POST['confsenha_usuario'] = $_POST['senha_usuario'];	

	if (isset($_FILES['foto_usuario_novo']) and !empty($_FILES['foto_usuario_novo'])) {
		$foto = $_FILES['foto_usuario_novo'];
		$foto_upload = new Profissional();
		$_POST['foto_usuario'] = $foto_upload->upload_imagem($foto);
	}


		$usuario = new Profissional(); 
		
		$usuario->altera_dados_profissional($_POST, $codigo);
		header("location:../?pgs=modal_atualiza_dados");
	}else{
		$dados_usuario = 'senhas diferentes';
		header("location:../?pgs=atualiza_profissional&erro1=".$dados_usuario."&erro2=".$dados_profissional);
	}

}else{
	var_dump($dados_usuario);
	var_dump($dados_profissional);
	header("location:../?pgs=atualiza_profissional&erro1=".$dados_usuario."&erro2=".$dados_profissional);
}
}
	


