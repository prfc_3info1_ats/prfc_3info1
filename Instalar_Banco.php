<?php

	$usuario   = "root";
	$senha     = "";
    $nome_banco= "3info1_prfc";


	try{

		$conexao = new PDO("mysql:host=localhost;", $usuario, $senha);
		$conexao->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);


	
        $sql = "CREATE DATABASE IF NOT EXISTS $nome_banco;";
        $conexao->exec($sql);


        $sql = "USE $nome_banco;";
        $conexao->exec($sql);

        $sql = "CREATE TABLE IF NOT EXISTS usuario(
			
			senha_usuario varchar(128),
			nome_usuario varchar(100),
			cod_usuario int PRIMARY KEY auto_increment NOT NULL,
			email_usuario varchar(100),
			tipo_usuario int,
			pergunta_chave varchar(100),
			resposta_chave varchar(100),
			status_ativacao int

			)";
        $conexao->exec($sql);


        $sql2 = "USE $nome_banco;";
        $conexao->exec($sql2);

        $sql2 = "CREATE TABLE IF NOT EXISTS artigo (
			cod_artigo int PRIMARY KEY auto_increment NOT NULL,
			data_artigo date,
			titulo_artigo varchar(500),
			imagem varchar(500),
			texto_artigo varchar(5000),
			tags varchar(100),
			cod_usuario_profissional int,
			FOREIGN KEY(cod_usuario_profissional) REFERENCES usuario (cod_usuario)
			)";
        $conexao->exec($sql2);


        $sql3 = "USE $nome_banco;";
        $conexao->exec($sql3);

        $sql3 = "CREATE TABLE IF NOT EXISTS avaliacao_artigo (
			cod_artigo int,
			cod_usuario int,
			cod_av_artigo int PRIMARY KEY auto_increment NOT NULL,
			classificacao_av_artigo int,
			FOREIGN KEY(cod_artigo) REFERENCES artigo (cod_artigo),
			FOREIGN KEY(cod_usuario) REFERENCES usuario (cod_usuario)
			)";
        $conexao->exec($sql3);


        $sql4 = "USE $nome_banco;";
        $conexao->exec($sql4);

        $sql4 = "CREATE TABLE IF NOT EXISTS profissional (
			crm varchar(11),
			area_atua varchar(100),
			foto_perfil varchar(500),
			cpf varchar(32),
			data_nasc date,
			cidade_atua varchar(100),
			telefone varchar(32),
			avaliacao_positiva int,
			avaliacao_negativa int,
			cod_usuario int,
			FOREIGN KEY(cod_usuario) REFERENCES usuario (cod_usuario)
			)";
        $conexao->exec($sql4);

       
		$sql10= "USE $nome_banco;";


		$conexao->exec($sql10);
		$sql10 = "CREATE TABLE IF NOT EXISTS avaliacao_profissional_usuario (
			avaliacao int,
			cod_usuario_avaliado int,
			cod_usuario_avaliando int,
			FOREIGN KEY(cod_usuario_avaliado) REFERENCES usuario (cod_usuario),
			FOREIGN KEY(cod_usuario_avaliando) REFERENCES usuario (cod_usuario)			
			)";
		$conexao->exec($sql10);


        

        
        $sql6 = "USE $nome_banco;";
        $conexao->exec($sql6);

        $sql6 = "CREATE TABLE IF NOT EXISTS mensagem (
			data_mensagem date,
			cod_usuario_remetente int,
			cod_usuario_destinatario int,
			texto_mensagem varchar(5000),
			cod_mensagem int PRIMARY KEY auto_increment NOT NULL,
			FOREIGN KEY(cod_usuario_remetente) REFERENCES usuario (cod_usuario),
			FOREIGN KEY(cod_usuario_destinatario) REFERENCES usuario (cod_usuario)
			)";
        $conexao->exec($sql6);
      	
      	$sql8 = "USE $nome_banco;";
        $conexao->exec($sql8);

        $sql8 = "CREATE TABLE IF NOT EXISTS comentario (
			cod_artigo int,
			cod_usuario int,
			cod_comentario_artigo int PRIMARY KEY auto_increment NOT NULL,
			data_comentario_artigo date,
			texto_comentario_artigo varchar(1000),
			cod_comentario int NULL,
			denunciado boolean,
			FOREIGN KEY(cod_artigo) REFERENCES artigo (cod_artigo),
			FOREIGN KEY(cod_usuario) REFERENCES usuario (cod_usuario),
			FOREIGN KEY(cod_comentario) REFERENCES comentario (cod_comentario_artigo)
			)";
        $conexao->exec($sql8);

        $sql5 = "USE $nome_banco;";
        $conexao->exec($sql5);

        $sql5 = "CREATE TABLE IF NOT EXISTS denuncia (
			cod_denuncia int NOT NULL PRIMARY KEY auto_increment,
			cod_artigo int NOT NULL,
			cod_denunciado int NOT NULL,
			cod_denunciador int NOT NULL,
			cod_comentario int NOT NULL,
			status_denuncia int NOT NULL,
			FOREIGN KEY(cod_artigo) REFERENCES artigo(cod_artigo),
			FOREIGN KEY(cod_denunciado) REFERENCES usuario(cod_usuario),
			FOREIGN KEY(cod_denunciador) REFERENCES usuario(cod_usuario),
			FOREIGN KEY(cod_comentario) REFERENCES comentario(cod_comentario_artigo)
			)";
        $conexao->exec($sql5);

        echo "Banco Criado!";

	$senha = password_hash('1234567a', PASSWORD_DEFAULT);
	
        $sql9 = "INSERT INTO usuario (nome_usuario, senha_usuario, email_usuario, tipo_usuario, pergunta_chave, resposta_chave, status_ativacao)
        		VALUES('Administrador', '$senha', 'administrador@gmail.com', 1 , 'Qual a data do aniversário da sua irmã?', '12', 1)";
        $conexao->exec($sql9);
        
	}catch(PDOException $erro){

		echo "Erro: " . $erro->getMessage();

	}

