<!DOCTYPE html>
<html lang="en">
	<head>
	    <meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

	    <title></title>
	    <link href="semantic/components/css_menu.css" rel="stylesheet" type="text/css">	    
	    <link href="css_js/MeuCSS.css" rel="stylesheet" type="text/css">	    
	    <link href="semantic/semantic.css" rel="stylesheet" type="text/css">
	    <link rel="stylesheet" type="text/css" href="semantic/semantic.min.css">
	    <script type="text/javascript" src="semantic/js/semantic.js"> </script>
	    <script type="text/javascript" src="semantic/semantic.js"> </script>


	</head>

	  	<body>
		    <header>
		        <?php
		        include_once 'controller.php';
		        include_once 'Models/Login.php';
		        $login = new Login();
		        $teste = $login-> menuLogado();
		         
		        $menu = [
		            'menun1'=>'?pgs=inicial',
		            'menun2'=>'?pgs=form_cadastro_usuario',
		            'menun3'=>'?pgs=form_login',

		            'menul1'=>'?pgs=inicial_usuario',
		            'menul2'=>'?pgs=busca_artigo&pg=1',
		            'menul3'=>'?pgs=visualizacao_profissional',
		            'menul4'=>'?pgs=mensagem',
		            'menul5'=>'?pgs=minhas_curtidas&pg=1',
		            'menul6'=>'?pgs=altera_dados_usuario',
		            'menul7'=>'?pgs=modal_excluir_usuario',
		            'menul8'=>'./Controllers/processa_logout.php',
		            

		            'menup1'=>'?pgs=meus_artigos_profissional&pg=1',
		            'menup2'=>'?pgs=form_artigo',
		            'menup3'=>'?pgs=mensagem',
		            'menup4'=>'?pgs=altera_dados_usuario',
		            'menup5'=>'?pgs=form_artigo',
		            'menup6'=>'?pgs=form_artigo',
		            'menup7'=>'Controllers/processa_logout.php',
		            'menup8'=>'?pgs=inicial_usuario',
		            'menup9'=>'?pgs=minhas_curtidas&pg=1',


		            'menuadm1'=>'?pgs=inicial',
		            'menuadm2'=>'?pgs=denuncias',
		            'menuadm3'=>'?pgs=usuarios_bloqueados',
		            'menuadm4'=>'Controllers/processa_logout.php',

		            ];

		            $tipo = $login->taLogado();
		            if ($tipo==true) {
		            	$tipo_user = $login->retorna_tipo();
		           }
		            

					if ($teste == true){
							
							if ($tipo_user == 2) {
								$template1 = getTemplate('menu_profissional.php');
					            $templateFinal = parseTemplate( $template1, $menu );
					            echo $templateFinal;
							
							}elseif ($tipo_user == 3) {
								$template1 = getTemplate('menu_leitor.php');
					            $templateFinal = parseTemplate( $template1, $menu );
					            echo $templateFinal;
							}else{
					            $template1 = getTemplate('menu_administrador.php');
					            $templateFinal = parseTemplate( $template1, $menu );
					            echo $templateFinal;
					        }
					 
					 }else{
					            $template1 = getTemplate('menu_nao_logado.php');
					            $templateFinal = parseTemplate( $template1, $menu );
					            echo $templateFinal;}
					            ?>
		    </header> 
		    <br>
		    <div class="ui  container">
		    <?php 
		      ins_dados(filter_input(INPUT_GET, 'pgs',FILTER_SANITIZE_ENCODED));
		    ?>
		    </div>

		</body>
		<p></p>

		
	<footer >
	

		</footer>
	
	
</html>



