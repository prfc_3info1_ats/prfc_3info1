<?php

function ins_dados($pgs) {
	
	if(isset($pgs)){

		
		$arquivo = "Views/".$pgs.".php";
		
		if(file_exists($arquivo)){
			include_once $arquivo;
		} else {
			echo "<h4>A pagina ".$pgs." não existe!</h4>";
		}


	} else {
		include_once "Views/inicial.php";
	}
}


function getTemplate($template) {
	if ( is_file( $template) ) { // verificando se o arq existe
		return file_get_contents( $template ); // retornando conteúdo do arquivo
	} else{
		return FALSE;
	}
}

function parseTemplate( $template, $vetor ) {	
	foreach ($vetor as $a => $b) { // recebemos um array com as tags
		$template = str_replace( '{'.$a.'}', $b, $template );
	}
	return $template; // retorno o html com conteúdo final
}
