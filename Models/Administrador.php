<?php
include_once "Database.php";
include_once "Usuario.php";
/**
* 
*/
class Administrador extends Usuario
{
	


    public function busca_todas_denuncias(){
    $conexao = Database::getConnection();

    $sql = "SELECT DISTINCT cod_comentario,cod_denunciado, cod_artigo, status_denuncia FROM denuncia ORDER BY cod_denuncia DESC;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    return $consulta;
    
}	
	public function conta_denuncia($codigo_comentario){
    $conexao = Database::getConnection();

    $sql = "SELECT count(cod_denuncia) FROM denuncia  WHERE denuncia.cod_comentario = $codigo_comentario;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    
    return $consulta;
    
}

public function busca_cod_comentarios_denunciados(){
    $conexao = Database::getConnection();

    $sql = "SELECT DISTINCT cod_comentario FROM denuncia ORDER BY cod_denuncia DESC;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    return $consulta;
    
}


public function cadastrar_status_denuncia($status, $codigo){
        
        $conexao = Database::getConnection();

        $sql = "UPDATE `denuncia` SET `status_denuncia`= $status WHERE cod_comentario = $codigo;";

        $conexao->exec($sql);

    }

public function busca_denuncia_status(){
    $conexao = Database::getConnection();

    $sql = "SELECT DISTINCT cod_comentario,cod_denunciado, cod_artigo, status_denuncia FROM denuncia WHERE status_denuncia = 1 ORDER BY cod_denuncia DESC ;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    
    return $consulta;
    
}

public function apagar_denuncias($codigo){
        $conexao = Database::getConnection();

        $apagar = "DELETE FROM denuncia WHERE cod_comentario = $codigo;";
        $conexao->exec($apagar);

      }

public function busca_denuncia($codigo_comentario, $codigo_denunciado, $codigo_denunciador){
    $conexao = Database::getConnection();

    $sql = "SELECT cod_denuncia FROM denuncia  WHERE denuncia.cod_comentario = $codigo_comentario and denuncia.cod_denunciado = $codigo_denunciado and denuncia.cod_denunciador = $codigo_denunciador;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    
    return $consulta;
    
}
public function busca_denuncias_cod_comentario($cod_comentario){
    $conexao = Database::getConnection();

    $sql = "SELECT cod_denuncia FROM denuncia WHERE cod_comentario = $cod_comentario;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetchAll(PDO::FETCH_ASSOC);
    return $consulta;
    
}   
public function apagar_denuncias_cod_artigo($codigo){
        $conexao = Database::getConnection();

        $apagar = "DELETE FROM denuncia WHERE cod_artigo= $codigo;";
        $conexao->exec($apagar);

      }




}