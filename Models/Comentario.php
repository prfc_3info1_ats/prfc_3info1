<?php
include_once "Database.php";

    class Comentario{

    	private $texto_comentario;
      private $data_comentario;

    	public function cadastra_comentario($texto, $data, $cod_artigo, $cod_usuario){
    		$conexao = Database::getConnection();

    		$sql = "INSERT INTO comentario (texto_comentario_artigo, data_comentario_artigo, cod_artigo, cod_usuario)
                           VALUES ('$texto', '$data', $cod_artigo, $cod_usuario);";
            $conexao->exec($sql);
    	}
      public function cadastra_resposta($texto, $data, $cod_artigo, $cod_usuario, $cod_comentario){
        $conexao = Database::getConnection();

        $sql = "INSERT INTO comentario (texto_comentario_artigo, data_comentario_artigo, cod_artigo, cod_usuario, cod_comentario)
                           VALUES ('$texto', '$data', $cod_artigo, $cod_usuario, $cod_comentario);";
            $conexao->exec($sql);
      }

    	public function mostra_comentario($id_artigo){
    		$conexao = Database::getConnection();

    		$sql = "SELECT texto_comentario_artigo, data_comentario_artigo, cod_artigo, cod_usuario, cod_comentario_artigo, cod_comentario FROM comentario WHERE cod_artigo = $id_artigo;";
    		$busca = $conexao->query($sql);
    		$comentario = $busca->fetchAll(PDO::FETCH_ASSOC);

    		return $comentario;

    	}

      public function apagar_comentario($codigo){
        $conexao = Database::getConnection();

        $apagar_resposta = "DELETE FROM comentario WHERE cod_comentario = $codigo;";
        $conexao->exec($apagar_resposta);

        $apagar = "DELETE FROM comentario WHERE cod_comentario_artigo = $codigo;";
        $conexao->exec($apagar);

      }

      public function busca_resposta_comentario($codigo){
        $conexao = Database::getConnection();

        $sql = "SELECT * from comentario WHERE cod_comentario= $codigo;";
        $busca = $conexao->query($sql);
        $resposta = $busca->fetchAll(PDO::FETCH_ASSOC);
        return $resposta;

      }

      public function busca_apenas_comentario(){
        $conexao = Database::getConnection();

        $sql = "SELECT * FROM comentario WHERE cod_comentario IS NULL;";
        $busca = $conexao->query($sql);
        $comentario = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $comentario;

      }

      public function busca_apenas_comentario_denuncia($cod_comentario_artigo){
        $conexao = Database::getConnection();

        $sql = "SELECT cod_usuario FROM comentario WHERE cod_comentario_artigo= $cod_comentario_artigo and cod_comentario IS NULL;";
        $busca = $conexao->query($sql);
        $comentario = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $comentario;

      }

      public function busca_resposta_comentario_denuncia($codigo_comentario_artigo){
        $conexao = Database::getConnection();

        $sql = "SELECT cod_usuario from comentario WHERE cod_comentario_artigo= $codigo_comentario_artigo;";
        $busca = $conexao->query($sql);
        $resposta = $busca->fetchAll(PDO::FETCH_ASSOC);
        return $resposta;

      }

      public function busca_texto_comentario($codigo_comentario_artigo){
        $conexao = Database::getConnection();

        $sql = "SELECT texto_comentario_artigo from comentario WHERE cod_comentario_artigo= $codigo_comentario_artigo;";
        $busca = $conexao->query($sql);
        $resposta = $busca->fetchAll(PDO::FETCH_ASSOC);
        return $resposta;

      }

      public function bloqueia_comentario_adm($valor, $cod_comentario){
        $conexao = Database::getConnection();

        $sql = "UPDATE comentario set denunciado = $valor where cod_comentario_artigo = $cod_comentario;";
        $busca = $conexao->query($sql);
      }
}

