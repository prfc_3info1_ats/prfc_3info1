<?php
include_once "Database.php"; 
include_once "Usuario.php";

class Profissional extends Usuario {
    
    private $cpf;
    private $data_nasc;
    private $crm;
    private $area_atuacao;
    private $cidade_atua;
    private $foto_usuario;
    private $telefone;

    



    public function cadastrar_usuario_profissional($codigo_user, $crm, $area, $foto, $cpf, $data, $cidade, $telefone){
        
        $conexao = Database::getConnection();


        $sql = "INSERT INTO Profissional (cod_usuario, crm, area_atua, foto_perfil, cpf, data_nasc, cidade_atua, telefone) 
        VALUES ($codigo_user, '$crm', '$area', '$foto', '$cpf', '$data', '$cidade', '$telefone');";

        $conexao->exec($sql);

    }

    public function cadastra_avaliacao_profissional($codigo_profissional, $codigo_usuario, $avaliacao_profissional){
        $conexao = Database::getConnection();

        $sql1 = "INSERT INTO avaliacao_profissional_usuario (cod_usuario_avaliado, cod_usuario_avaliando, avaliacao) 
        VALUES ('$codigo_profissional', '$codigo_usuario', '$avaliacao_profissional')";
        $conexao->exec($sql1);

        if ($avaliacao_profissional == 1) {
            $sql2 = "SELECT  avaliacao_positiva FROM profissional, usuario WHERE profissional.cod_usuario = usuario.cod_usuario and profissional.cod_usuario = $codigo_profissional;";
            $n_avaliacao = $conexao->query($sql2);
            $avaliacao_positiva = $n_avaliacao->fetch(PDO::FETCH_ASSOC);
            
            $soma_avaliacao = $avaliacao_positiva['avaliacao_positiva'] + 1;           

            $sql3 = "UPDATE profissional SET avaliacao_positiva = '$soma_avaliacao' WHERE profissional.cod_usuario =$codigo_profissional;";
            $conexao->exec($sql3);
            
        }elseif ($avaliacao_profissional == 0) {
            $sql2 = "SELECT  avaliacao_negativa FROM profissional, usuario WHERE profissional.cod_usuario = usuario.cod_usuario and profissional.cod_usuario = $codigo_profissional;";
            $n_avaliacao = $conexao->query($sql2);
            $avaliacao_positiva = $n_avaliacao->fetch(PDO::FETCH_ASSOC);
            
            $soma_avaliacao = $avaliacao_positiva['avaliacao_negativa'] + 1; 
            

            $sql3 = "UPDATE profissional SET avaliacao_negativa = '$soma_avaliacao' WHERE profissional.cod_usuario = $codigo_profissional;";
            $conexao->exec($sql3);      
        }

        header('location:../?pgs=perfil_profissional&pg=1&id='. $codigo_profissional);
    }


    public function busca_avaliacao_profissional_positiva($id_usuario){
        $conexao = Database::getConnection();

        $avaliacao_positiva = "SELECT  avaliacao_positiva FROM profissional, usuario WHERE profissional.cod_usuario = usuario.cod_usuario and profissional.cod_usuario = $id_usuario;";
        $mostra_avaliacao_positiva = $conexao->query($avaliacao_positiva);
        $avaliacao = $mostra_avaliacao_positiva->fetch(PDO::FETCH_ASSOC);
        
        return $avaliacao["avaliacao_positiva"];
        
    }
    public function busca_avaliacao_profissional_negativa($id_usuario){
        $conexao = Database::getConnection();

        $avaliacao_negativa = "SELECT  avaliacao_negativa FROM profissional, usuario WHERE profissional.cod_usuario = usuario.cod_usuario and profissional.cod_usuario = $id_usuario;";

        $mostra_avaliacao_negativa = $conexao->query($avaliacao_negativa);
        $avaliacao = $mostra_avaliacao_negativa->fetch(PDO::FETCH_ASSOC);
        
        return $avaliacao["avaliacao_negativa"];
        
    }

    public function busca_todos_profissionais(){
        $conexao = Database::getConnection();

        $select = "SELECT  usuario.cod_usuario, nome_usuario, area_atua, foto_perfil FROM profissional, usuario where  profissional.cod_usuario = usuario.cod_usuario  AND usuario.tipo_usuario = 2;";
        $busca = $conexao->query($select);
        $profissionais = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $profissionais;
    }

    public function busca_dados($codigo){
      $conexao = Database::getConnection();

      $select = "SELECT  usuario.cod_usuario, nome_usuario, crm, area_atua, foto_perfil, cpf, data_nasc, cidade_atua, telefone FROM usuario, profissional where usuario.cod_usuario=profissional.cod_usuario and profissional.cod_usuario = $codigo;";
      $busca = $conexao->query($select);
      $usuario = $busca->fetchAll(PDO::FETCH_ASSOC);

      return $usuario;
     }
  
       public function apagar_avaliacao($id_profissional, $id_usuario){
        $conexao = Database::getConnection();

        $apagar = "SELECT apagar_avaliacao($id_profissional, $id_usuario); ";
        $conexao->exec($apagar);

        header('location:../?pgs=perfil_profissional&pg=1&id='. $id_profissional);
    }


    public function altera_dados_profissional($dados, $codigo){
       extract($dados);

       $dados_padrao = array (
        "nome_usuario" =>$nome_usuario, 
        "email_usuario"=>$email_usuario, 
        "senha_usuario"=>$senha_usuario, 
        "rec_senha"=>$rec_senha, 
        "rec_senha_resposta"=>$rec_senha_resposta
        );

       parent::atualiza_cadastro($dados_padrao, $codigo);

       $conexao = Database::getConnection();
       

       $atualiza = "UPDATE profissional SET crm = '$crm', area_atua = '$area', foto_perfil = '$foto_usuario', cpf='$cpf', data_nasc = '$data', cidade_atua = '$cidade', telefone = '$tel' WHERE cod_usuario = $codigo;";

       $conexao->exec($atualiza);
    }
    

    public function pesquisa_profissionais($pesquisa){

        $conexao = Database::getConnection();
        $sql = "SELECT DISTINCT usuario.cod_usuario, nome_usuario, crm, foto_perfil, area_atua FROM usuario, profissional where usuario.cod_usuario = profissional.cod_usuario and tipo_usuario = 2 and (nome_usuario like '%{$pesquisa}%' or area_atua like '%{$pesquisa}%')
        ;";
        $busca = $conexao->query($sql);
        $profissionais = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $profissionais;  

    } 

    public function busca_profissionais_limite($limite){
        $conexao = Database::getConnection();

        $select = "SELECT DISTINCT usuario.cod_usuario, nome_usuario, area_atua, foto_perfil, crm FROM profissional, usuario where profissional.cod_usuario = usuario.cod_usuario and tipo_usuario = 2 ORDER BY usuario.cod_usuario DESC LIMIT $limite;";
        $busca = $conexao->query($select);
        $profissionais = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $profissionais;
    }

    public function busca_curtidas_profissional($id_usuario){
        $conexao = Database::getConnection();

        $avaliacao_positiva = "SELECT cod_usuario_avaliado FROM avaliacao_profissional_usuario WHERE avaliacao = 1 and cod_usuario_avaliando = $id_usuario;";
        $mostra_avaliacao_positiva = $conexao->query($avaliacao_positiva);
        $avaliacao = $mostra_avaliacao_positiva->fetchAll(PDO::FETCH_ASSOC);
        
        return $avaliacao;
        
    }


    public function valida_dados_profissional($telefone, $cpf, $data){
   
        $erro = false;
        $data_atual = date('Y');

       $ano= explode('-', $data);
       $idade = $data_atual - $ano[0];
        if ($idade<= 21) {
            $erro = "Você não tem idade para ser um profissional";
        }
        
        if(!preg_match("^([0-9]){3}\.([0-9]){3}\.([0-9]){3}-([0-9]){2}$^", $cpf)) {
         $erro = "CPF inválido.";
        }

        if ( $erro ) {
         return $erro;
        } else {
            return false;
         
        }
    }
    
    public function atualiza_foto_perfil($caminho_foto){
        $conexao = Database::getConnection();
        $sql = "UPDATE profissional SET avaliacao_negativa = '$soma_avaliacao' WHERE profissional.cod_usuario = $codigo_profissional;";
            $conexao->exec($sql);     
    }

    public function upload_imagem($foto_usuario){
        // Largura máxima em pixels
        $largura = 10240;
        // Altura máxima em pixels
        $altura = 10240;
        // Tamanho máximo do arquivo em bytes
        $tamanho = 1000000000;
 
        $error = array();
 
        // Verifica se o arquivo é uma imagem
        if(!preg_match("/^image\/(pjpeg|jpeg|png|gif|bmp)$/", $foto_usuario["type"])){
           $error[1] = "Isso não é uma imagem.";
        } 
    
        // Pega as dimensões da imagem
        $dimensoes = getimagesize($foto_usuario["tmp_name"]);
    
        // Verifica se a largura da imagem é maior que a largura permitida
        if($dimensoes[0] > $largura) {
            $error[2] = "A largura da imagem não deve ultrapassar ".$largura." pixels";
        }
 
        // Verifica se a altura da imagem é maior que a altura permitida
        if($dimensoes[1] > $altura) {
            $error[3] = "Altura da imagem não deve ultrapassar ".$altura." pixels";
        }
        
        // Verifica se o tamanho da imagem é maior que o tamanho permitido
        if($foto_usuario["size"] > $tamanho) {
            $error[4] = "A imagem deve ter no máximo ".$tamanho." bytes";
        }
        // Se não houver nenhum erro
        if (count($error) == 0) {
        
            // Pega extensão da imagem
            preg_match("/\.(gif|bmp|png|jpg|jpeg){1}$/i", $foto_usuario["name"], $ext);
 
            // Gera um nome único para a imagem
            $nome_imagem = md5(uniqid(time())) . "." . $ext[1];
 
            // Caminho de onde ficará a imagem
            $caminho_imagem = "../imagens/" . $nome_imagem;
 
            // Faz o upload da imagem para seu respectivo caminho
            move_uploaded_file($foto_usuario["tmp_name"], $caminho_imagem);

            return $nome_imagem;


            
        }
        //Se houver mensagens de erro, exibe-as
            if (count($error) != 0) {
                foreach ($error as $erro) {
                    echo $erro . "<br />";
                }
            }   

    }




  /**
     * @return mixed
     */
  public function getCpf()
  {
    return $this->cpf;
}

    /**
     * @param mixed $cpf
     */
    public function setCpf($cpf)
    {
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getDataNasc()
    {
        return $this->data_nasc;
    }

    /**
     * @param mixed $data_nasc
     */
    public function setDataNasc($data_nasc)
    {
        $this->data_nasc = $data_nasc;
    }

    /**
     * @return mixed
     */
    public function getCrm()
    {
        return $this->crm;
    }

    /**
     * @param mixed $crm
     */
    public function setCrm($crm)
    {
        $this->crm = $crm;
    }

    /**
     * @return mixed
     */
    public function getAreaAtuacao()
    {
        return $this->area_atuacao;
    }

    /**
     * @param mixed $area_atuacao
     */
    public function setAreaAtuacao($area_atuacao)
    {
        $this->area_atuacao = $area_atuacao;
    }

    /**
     * @return mixed
     */
    public function getCidadeAtua()
    {
        return $this->cidade_atua;
    }

    /**
     * @param mixed $cidade_atua
     */
    public function setCidadeAtua($cidade_atua)
    {
        $this->cidade_atua = $cidade_atua;
    }

    /**
     * @return mixed
     */
    public function getFotoUsuario()
    {
        return $this->foto_usuario;
    }

    /**
     * @param mixed $foto_usuario
     */
    public function setFotoUsuario($foto_usuario)
    {
        $this->foto_usuario = $foto_usuario;
    }

    /**
     * @return mixed
     */
    public function getTelefone()
    {
        return $this->telefone;
    }

    /**
     * @param mixed $telefone
     */
    public function setTelefone($telefone)
    {
        $this->telefone = $telefone;
    }

    /**
     * @return mixed
     */
    public function getNomeUsuario()
    {
        return $this->nome_usuario;
    }

    /**
     * @param mixed $nome_usuario
     */
    public function setNomeUsuario($nome_usuario)
    {
        $this->nome_usuario = $nome_usuario;
    }

    /**
     * @return mixed
     */
    public function getEmailUsuario()
    {
        return $this->email_usuario;
    }

    /**
     * @param mixed $email_usuario
     */
    public function setEmailUsuario($email_usuario)
    {
        $this->email_usuario = $email_usuario;
    }

    /**
     * @return mixed
     */
    public function getSenhaUsuario()
    {
        return $this->senha_usuario;
    }

    /**
     * @param mixed $senha_usuario
     */
    public function setSenhaUsuario($senha_usuario)
    {
        $this->senha_usuario = $senha_usuario;
    }

    /**
     * @return mixed
     */
    public function getTipoUsuario()
    {
        return $this->tipo_usuario;
    }

    /**
     * @param mixed $tipo_usuario
     */
    public function setTipoUsuario($tipo_usuario)
    {
        $this->tipo_usuario = $tipo_usuario;
    }

    /**
     * @return mixed
     */
    public function getRecSenhaPergunta()
    {
        return $this->rec_senha_pergunta;
    }

    /**
     * @param mixed $rec_senha_pergunta
     */
    public function setRecSenhaPergunta($rec_senha_pergunta)
    {
        $this->rec_senha_pergunta = $rec_senha_pergunta;
    }

    /**
     * @return mixed
     */
    public function getRecSenhaResposta()
    {
        return $this->rec_senha_resposta;
    }

    /**
     * @param mixed $rec_senha_resposta
     */
    public function setRecSenhaResposta($rec_senha_resposta)
    {
        $this->rec_senha_resposta = $rec_senha_resposta;
    }   
}