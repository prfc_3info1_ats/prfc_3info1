<?php
include_once "Database.php"; 
include_once "Logout.php"; 


class Usuario {
    

    protected $nome_usuario;
    protected $email_usuario;
    protected $senha_usuario;
    protected $tipo_usuario;
    protected $rec_senha_pergunta;
    protected $rec_senha_resposta;
    protected $status_ativacao;




    public function cadastrar_usuario($nome, $email, $senha, $tipo, $pergunta, $resposta){
        $conexao = Database::getConnection();

        $sql = "INSERT INTO usuario (nome_usuario, email_usuario, senha_usuario, tipo_usuario, pergunta_chave, resposta_chave, status_ativacao) 
        VALUES ('$nome', '$email', '$senha', '$tipo', '$pergunta', '$resposta', 1);";

        $conexao->exec($sql);        
    }

    public function busca_dados($codigo){
      $conexao = Database::getConnection();

      $select = "SELECT  senha_usuario, nome_usuario, cod_usuario, email_usuario, tipo_usuario, pergunta_chave, resposta_chave FROM usuario where  cod_usuario = $codigo;";
      $busca = $conexao->query($select);
      $usuario = $busca->fetchAll(PDO::FETCH_ASSOC);

      return $usuario;
  }

  public function atualiza_cadastro($dados, $codigo){
    $conexao = Database::getConnection();

    extract($dados);
    

    $atualiza = "UPDATE usuario SET nome_usuario = '$nome_usuario', email_usuario = '$email_usuario', senha_usuario = '$senha_usuario', pergunta_chave = '$rec_senha', resposta_chave = '$rec_senha_resposta' WHERE cod_usuario = $codigo;";

    $conexao->exec($atualiza);
}

public function altera_status_usuario($status, $codigo){
       $conexao = Database::getConnection();

       $altera = "UPDATE usuario SET status_ativacao= $status WHERE usuario.cod_usuario = $codigo;";
       $conexao->exec($altera);

       if ($codigo = 0) {
        $logout = new Logout();
        $logout->logout();
       }
       
    }

public function verifica_ativacao_usuario($codigo){
        $conexao = Database::getConnection();
        $verifica = "SELECT status_ativacao FROM usuario WHERE usuario.cod_usuario = $codigo;";
        $executa = $conexao->query($verifica);
        $verificacao = $executa->fetch(PDO::FETCH_ASSOC);
        
        if ($verificacao == null) {
            return false;
            
        }else{
            return $verificacao['status_ativacao'];
        }

    }


public function busca_nome_usuario($id_usuario){
    $conexao = Database::getConnection();

    $sql = "SELECT nome_usuario FROM usuario WHERE cod_usuario = $id_usuario;";
    $busca = $conexao->query($sql);
    $consulta = $busca->fetch(PDO::FETCH_ASSOC);

    return $consulta['nome_usuario'];
}

public function cadastra_denuncia($cod_denunciado, $cod_denunciador, $cod_artigo, $cod_comentario){
        $conexao = Database::getConnection();

        $sql = "INSERT INTO denuncia (cod_artigo, cod_denunciado, cod_denunciador, cod_comentario) 
        VALUES ('$cod_artigo', '$cod_denunciado', '$cod_denunciador', '$cod_comentario');";

        $conexao->exec($sql);        
    }


public function valida_dados($dados){
    if (isset($dados['senha_usuario'])) {
       $senha = $dados['senha_usuario'];
    }
    $email = $dados['email_usuario'];
    $nome = $dados['nome_usuario'];


        $conexao = Database::getConnection();

        $sql = "SELECT email_usuario FROM usuario WHERE email_usuario = '$email';";
        $busca = $conexao->query($sql);
        $consulta = $busca->fetch(PDO::FETCH_ASSOC);
        if (isset($consulta['email_usuario']) and !is_null($consulta['email_usuario'])) {
            $erro = 'esse email já foi cadastrado';
        }

        $erro = false;
         
        foreach ( $dados as $chave => $valor ) {
           $chave = trim($valor);
           $chave = htmlspecialchars($chave);
           $chave = stripslashes($chave);
           $chave = filter_var($chave, FILTER_SANITIZE_STRIPPED);

         if ( empty ( $valor ) ) {
         $erro = 'Existem campos em branco.';
         }
        }

        if (preg_match('/[\'^£$%&*()}{@#~?><>,|=_+¬-]/', $nome))
        {
           $erro = 'o nome deve conter apenas letras';
        }
        if (isset($senha)) {
            if(!preg_match('/^(?=.*\d)(?=.*[A-Za-z])[0-9A-Za-z]{8,12}$/', $senha) && !$erro ) {
            $erro = "senha deve ter no minimo 8 e no máximo 12 caracteres e deve conter letras e números";
            }
        }
        if ( ( ! isset( $email ) || ! filter_var( $email, FILTER_VALIDATE_EMAIL ) ) && !$erro ) {
         $erro = 'Envie um email válido.';
        }
         
        if ( $erro ) {
         return $erro;
        } else {
            return $dados;
         
        }
    }

    public function busca_tipo_usuario($codigo){
      $conexao = Database::getConnection();

      $select = "SELECT  tipo_usuario FROM usuario where  cod_usuario = $codigo;";
      $busca = $conexao->query($select);
      $usuario = $busca->fetchAll(PDO::FETCH_ASSOC);

      return $usuario;
  }

    
    /**
     * @return mixed
     */
    public function getNomeUsuario()
    {
        return $this->nome_usuario;
    }

    /**
     * @param mixed $nome_usuario
     */
    public function setNomeUsuario($nome)
    {
        $this->nome_usuario = $nome;
    }

    /**
     * @return mixed
     */
    public function getEmailUsuario()
    {
        return $this->email_usuario;
    }

    /**
     * @param mixed $email_usuario
     */
    public function setEmailUsuario($email_usuario)
    {
        $this->email_usuario = $email_usuario;
    }

    /**
     * @return mixed
     */
    public function getSenhaUsuario()
    {
        return $this->senha_usuario;
    }

    /**
     * @param mixed $senha_usuario
     */
    public function setSenhaUsuario($senha_usuario)
    {
        $this->senha_usuario = $senha_usuario;
    }

    /**
     * @return mixed
     */
    public function getTipoUsuario()
    {
        return $this->tipo_usuario;
    }

    /**
     * @param mixed $tipo_usuario
     */
    public function setTipoUsuario($tipo_usuario)
    {
        $this->tipo_usuario = $tipo_usuario;
    }

    /**
     * @return mixed
     */
    public function getRecSenhaPergunta()
    {
        return $this->rec_senha_pergunta;
    }

    /**
     * @param mixed $rec_senha_pergunta
     */
    public function setRecSenhaPergunta($rec_senha_pergunta)
    {
        $this->rec_senha_pergunta = $rec_senha_pergunta;
    }

    /**
     * @return mixed
     */
    public function getRecSenhaResposta()
    {
        return $this->rec_senha_resposta;
    }

    /**
     * @param mixed $rec_senha_resposta
     */
    public function setRecSenhaResposta($rec_senha_resposta)
    {
        $this->rec_senha_resposta = $rec_senha_resposta;
    }

}



