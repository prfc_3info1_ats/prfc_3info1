<?php

include_once "Database.php";

class Artigo {

    private $data_artigo;
    private $titulo_artigo;
    private $imagem;
    private $texto_artigo;
    private $tags;

    
    


    public function cadastrar_artigo($data_artigo, $titulo_artigo, $imagem, $texto_artigo, $tags, $codigo_profissional){

        $conexao = Database::getConnection();

        $sql = "INSERT INTO Artigo (data_artigo, titulo_artigo, imagem, texto_artigo, tags, cod_usuario_profissional) 
                VALUES ('$data_artigo', '$titulo_artigo', '$imagem', '$texto_artigo', '$tags', '$codigo_profissional');";

        $conexao->exec($sql);
    }

    public function cadastra_avaliacao_artigo($codigo_artigo, $codigo_usuario, $avaliacao_artigo){
        $conexao = Database::getConnection();

        $sql = "INSERT INTO avaliacao_artigo (cod_artigo, cod_usuario, classificacao_av_artigo) 
        VALUES ('$codigo_artigo', '$codigo_usuario', '$avaliacao_artigo');";
        
        $conexao->exec($sql);

        header('location:../?pgs=busca_artigo');
    }


    public function busca_avaliacao_artigo($n_avaliacao, $cod_artigo){
        $conexao = Database::getConnection();

        $avaliacao_positiva = "SELECT count(cod_av_artigo) FROM avaliacao_artigo WHERE classificacao_av_artigo = $n_avaliacao and cod_artigo = $cod_artigo;";
        $mostra_avaliacao_positiva = $conexao->query($avaliacao_positiva);
        $avaliacao = $mostra_avaliacao_positiva->fetch(PDO::FETCH_ASSOC);
        
        return $avaliacao["count(cod_av_artigo)"];
        
    }

    public function resumo_texto($texto, $comprimento){

        return rtrim(substr($texto, 0, $comprimento ))."...";
        
    }

    public function apagar_avaliacao($codigo, $cod_artigo){
        $conexao = Database::getConnection();

        $apagar = "DELETE FROM avaliacao_artigo WHERE cod_usuario = $codigo and cod_artigo=$cod_artigo;";
        $conexao->exec($apagar);
    }

    public function busca_todos_artigos(){
        $conexao = Database::getConnection();

        $busca = "SELECT * FROM artigo;";
        $busca_artigos = $conexao->query($busca);
        $todos_artigos = $busca_artigos->fetchAll(PDO::FETCH_ASSOC);
        
        return $todos_artigos;
        
    }

    public function busca_artigo($id){
        $conexao = Database::getConnection();

        $busca = "SELECT * FROM artigo WHERE cod_artigo = $id;";
        $artigo = $conexao->query($busca);
        $retorno = $artigo->fetchAll(PDO::FETCH_ASSOC);

        return $retorno;

    }

    public function busca_artigos_limite($limite){
        $conexao = Database::getConnection();

        $busca = "SELECT * FROM artigo ORDER BY cod_artigo DESC LIMIT $limite;";
        $busca_artigos = $conexao->query($busca);
        $todos_artigos = $busca_artigos->fetchAll(PDO::FETCH_ASSOC);
        
        return $todos_artigos;
        
    }

    public function atualiza_artigo($dados, $codigo_artigo){
        $conexao = Database::getConnection();

        extract($dados);
        $atualiza = "UPDATE artigo SET titulo_artigo = '$titulo_artigo', imagem = '$fotoartigo', texto_artigo = '$text_artigo', tags = '$tags' WHERE cod_artigo = $codigo_artigo;";
        $conexao->exec($atualiza);
    }

    public function conta_artigo(){
        $conexao = Database::getConnection();
        $busca = "SELECT * FROM artigo;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $tamanho = count($retorno);
        return $tamanho;
    }
    
    public function quantidade_paginas_artigo($quantidade){
        $objeto = new Artigo();
        $tamanho =$objeto-> conta_artigo();
        $n_paginas = $tamanho / $quantidade;
        $pg_arredondado = ceil($n_paginas);

        return $pg_arredondado;
    }
    

    public function paginacao_artigo($pagina_escolhida, $quantidade){

        $pagina = $pagina_escolhida - 1;
        $inicio = $pagina * $quantidade;

        $conexao = Database::getConnection();
        $busca = "SELECT * FROM artigo ORDER BY cod_artigo DESC LIMIT $quantidade OFFSET $inicio;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $pagina_esc = new Artigo();
        $pg_arredondado = $pagina_esc-> quantidade_paginas_artigo($quantidade);
        


        if ($pagina_escolhida<=$pg_arredondado) {
          while ($pagina_escolhida<=$pg_arredondado) {
            return $retorno;
          }
        }else{
            echo "Essa página não existe";
        }                           
}


    public function conta_artigo_profissional($codigo){
        $conexao = Database::getConnection();
        $busca = "SELECT * FROM artigo WHERE cod_usuario_profissional = $codigo;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $tamanho = count($retorno);
        return $tamanho;
    }
    
    public function quantidade_paginas_artigo_profissional($quantidade, $codigo){
        $objeto = new Artigo();
        $tamanho =$objeto-> conta_artigo_profissional($codigo);
        $n_paginas = $tamanho / $quantidade;
        $pg_arredondado = ceil($n_paginas);

        return $pg_arredondado;
    }
    

    public function paginacao_artigo_profissional($pagina_escolhida, $quantidade, $codigo){

        $pagina = $pagina_escolhida - 1;
        $inicio = $pagina * $quantidade;

        $conexao = Database::getConnection();
        $busca = "SELECT * FROM artigo  WHERE cod_usuario_profissional = $codigo ORDER BY cod_artigo DESC LIMIT $quantidade OFFSET $inicio;";
        $resultado = $conexao->query($busca);
        $retorno = $resultado->fetchAll(PDO::FETCH_ASSOC);
        
        $pagina_esc = new Artigo();
        $pg_arredondado = $pagina_esc-> quantidade_paginas_artigo_profissional($quantidade, $codigo);
        


        if ($pagina_escolhida<=$pg_arredondado) {
          while ($pagina_escolhida<=$pg_arredondado) {
            return $retorno;
          }
        }else{
            ?>

            <div class="ui compact segment">
                <p>Não há artigos.</p>
            </div>
            <?php
        }                         
}
    

    public function busca_curtidas($id_usuario){
        $conexao = Database::getConnection();
        $sql ="SELECT  artigo.cod_artigo, data_artigo, titulo_artigo, imagem, texto_artigo, tags, artigo.cod_usuario_profissional from artigo, usuario, avaliacao_artigo 
            where usuario.cod_usuario=avaliacao_artigo.cod_usuario 
            and avaliacao_artigo.cod_artigo = artigo.cod_artigo
            and avaliacao_artigo.cod_usuario = $id_usuario 
            and classificacao_av_artigo = 1;";
        $busca = $conexao->query($sql);
        $dados = $busca->fetchAll(PDO::FETCH_ASSOC);
        return $dados;

    }


        


    public function pesquisa_artigo($pesquisa){
        $conexao = Database::getConnection();
        $sql = "SELECT cod_artigo, data_artigo, titulo_artigo, imagem, cod_usuario_profissional from artigo where texto_artigo like '%{$pesquisa}%' or titulo_artigo like '%{$pesquisa}%' or tags like '%{$pesquisa}%';";
        $busca = $conexao->query($sql);
        $artigos = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $artigos; 
    } 


    public function buscar_foto_profissional($codigo_profissional){
        $conexao = Database::getConnection();
        $sql = "SELECT foto_perfil from profissional where cod_usuario = $codigo_profissional;";
        $busca = $conexao->query($sql);
        $foto = $busca->fetchAll(PDO::FETCH_ASSOC);

        return $foto;
    }

    public function buscar_area_profissional($codigo_profissional){
        $conexao = Database::getConnection();
        $sql = "SELECT area_atua from profissional where cod_usuario = $codigo_profissional;";
        $busca = $conexao->query($sql);
        $area = $busca->fetch(PDO::FETCH_ASSOC);

        return $area;
    }


    public function apagar_artigo($codigo_artigo){
            $conexao = Database::getConnection();

            $apagar1 = "DELETE FROM avaliacao_artigo WHERE cod_artigo = $codigo_artigo;";
            $conexao->exec($apagar1);

            $sql= "SELECT cod_comentario_artigo FROM comentario WHERE cod_artigo = $codigo_artigo;";
            $retorno = $conexao->query($sql);
            $busca = $retorno->fetchAll(PDO::FETCH_ASSOC);

            foreach ($busca as $cod_comentario) {
               foreach ($cod_comentario as $codigo) {
                   $apagar2="DELETE FROM comentario WHERE cod_comentario= $codigo;";
                   $conexao->exec($apagar2);
               }
            }

            $apagar3 = "DELETE FROM comentario WHERE cod_artigo = $codigo_artigo;";
            $conexao->exec($apagar3);

            $apagar4= "DELETE FROM artigo WHERE cod_artigo = $codigo_artigo;";
            $conexao->exec($apagar4);

            
            header('location:../?pgs=modal_artigo_excluido');
        }



/**
     * @return mixed
     */
public function getDataArtigo()
{
    return $this->data_artigo;
}

    /**
     * @param mixed $data_artigo
     */
    public function setDataArtigo($data_artigo)
    {
        $this->data_artigo = $data_artigo;
    }

    /**
     * @return mixed
     */
    public function getTituloArtigo()
    {
        return $this->titulo_artigo;
    }

    /**
     * @param mixed $titulo_artigo
     */
    public function setTituloArtigo($titulo_artigo)
    {
        $this->titulo_artigo = $titulo_artigo;
    }

    /**
     * @return mixed
     */
    public function getImagem()
    {
        return $this->imagem;
    }

    /**
     * @param mixed $imagem
     */
    public function setImagem($imagem)
    {
        $this->imagem = $imagem;
    }

    /**
     * @return mixed
     */
    public function getTextoArtigo()
    {
        return $this->texto_artigo;
    }

    /**
     * @param mixed $texto_artigo
     */
    public function setTextoArtigo($texto_artigo)
    {
        $this->texto_artigo = $texto_artigo;
    }

    /**
     * @return mixed
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param mixed $tags
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }
    
}




