<?php 


class Database{
	
	public static function getConnection(){
		try{
			$conexao = new PDO("mysql:host=localhost;dbname=3info1_prfc", 'root', '');
			$conexao-> setAttribute (PDO::ATTR_ERRMODE, PDO:: ERRMODE_EXCEPTION);
			return $conexao;
		}
		
		catch(PDOException $erro){
        	echo "Conexao falhou: ". $erro->getMessage();
        }
    }
}