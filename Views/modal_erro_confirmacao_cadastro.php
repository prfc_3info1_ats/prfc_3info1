<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="remove icon"></i>
        A confirmação de senha está diferente.
      </div>
      <div class="actions">
        <a class="ui button" href="?pgs=form_cadastro_usuario">Voltar</a>
      </div>
    </div>
  </div>

</body>
</html>