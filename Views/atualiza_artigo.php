<?php include_once "./Models/Usuario.php";
      include_once "./Models/Login.php"; 
      include_once "./Models/Artigo.php";
      include_once "./Models/Profissional.php";


            $logado = new Login();
            $esta_logado=$logado->taLogado();
            $codigo = $logado->retorna_codigo_usuario();
            $tipo = $logado->retorna_tipo(); 
            if ($esta_logado == false) {
              header('location:?pgs=inicial');
            }
            if ($tipo == 1 && $tipo == 3) {
              header('location:?pgs=inicial_usuario');
              exit();
            }                   
            $id = $_GET['id'];

            $artigo = new artigo();
            $informacoes_artigo = $artigo->busca_artigo($id);

            $informacao= $informacoes_artigo[0]; 

            ?>


<body>

    <?php    
  if ($tipo == 2 ){?>
    <div class="ui centered grid">
      <div class="eight wide column">
        <h2 class="ui horizontal divider header">
          <div class="content">
            Atualizar Artigo 
          </div>
        </h2>
        <form class='ui form' method='post' action="./Controllers/processa_atualizacao_artigo.php" enctype="multipart/form-data">
          <div class="ui stacked secondary  segment">
            <div class="field">
              <label>Título</label>
                <input type="text" name="titulo_artigo" value ='<?=$informacao['titulo_artigo'];?>'>
            </div>
            <div class="field">
              <label>Texto</label>
                <textarea type="text" name="text_artigo" rows="10" cols="40" value='<?=$informacao['texto_artigo'];?>' ><?=$informacao['texto_artigo'];?></textarea>
            </div>
            <div class="field">
              <label>Foto</label>
                <input type="file" class ="fotoartigo" name="fotoartigo_novo" accept="image/*" value='<?=$informacao["imagem"];?>'>
                <input type='hidden' name="fotoartigo" accept="image/*" value='<?=$informacao["imagem"];?>' >
            </div>
            <div class="field">
              <label>Tags</label>
                <input type="text" name="tags" value='<?=$informacao['tags'];?>'>
            </div>
            <input type="hidden" value='<?php echo $id;?>' name="id">
            <button type="submit" name="Atualizar" class="ui fluid large grey submit button">Atualizar</button>

          </div>
        </form>
      <?php 
        }else{
        echo "Usuário não pode acessar a página";
          }
      
    ?>

  </div>
</div>

</body>

