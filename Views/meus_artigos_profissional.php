<body>

  <div class="ui container">
    <div class="ui stackable centered grid">
      <div class="twelve wide column">
        <p></p>
        <h2 class="ui horizontal divider header">
          <i class="newspaper icon"></i>
          Meus Artigos
        </h2>
        <p></p>

        <?php
        include_once "./Models/Artigo.php"; 
        include_once "./Models/Usuario.php";
        include_once "./Models/Login.php";
        include_once "./Models/Profissional.php";

        $limite_titulo = new Artigo();
        $pg_atual = $_GET['pg'];
        $pagina = new Artigo();
        $quantidade = 4;
        $tipo = new Login();
        $tipo_usuario = $tipo->retorna_tipo();

        if ($tipo_usuario == 2) {
          $retorna_profissional = new Login();
          $retorna_id = $retorna_profissional->retorna_codigo_usuario();
          $codigo_usuario = $retorna_id;
        }else{
          $codigo_usuario = $_GET['id'];
        }

        $artigos_paginacao = $pagina->paginacao_artigo_profissional($pg_atual, $quantidade, $codigo_usuario);

        $n_pagina = new Artigo();
        $limite = $n_pagina->quantidade_paginas_artigo_profissional($quantidade, $codigo_usuario);

        $anterior = $pg_atual -1;
        $proxima = $pg_atual + 1;
        if ($pg_atual=1) {
          $anterior = $pg_atual;
        }
      
        if ($_GET['pg']>$limite && $_GET['pg'] != 1) {
          ?>
          <p></p>
          <a href='?pgs=meus_artigos_profissional&pg=<?=$anterior;?>' class="">Voltar para a primeira página</a>
          <?php
          exit();
        }
       if (isset($artigos_paginacao)) {
         
        foreach ($artigos_paginacao as $artigo) {?>



          <div class="ui card meu-card">

            <div class="content">
            <div class="ui grid">
              <div class="four wide column">
                <a href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo'];?>>
                  <img class="ui medium rounded image" src="./imagens/<?=$artigo['imagem']?>">
                </a>
              </div>
              <div class="twelve wide column">
                <div class="right floated date"><?=$artigo['data_artigo'] ?></div>
                <h3>
                  <a href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo'];?>>
                    <div class="header"><?=$limite_titulo->resumo_texto($artigo['titulo_artigo'], 20); ?></div>
                  </a>
                </h3>
                    <div class="meta">
                      <a class="group" href='?pgs=perfil_profissional&pg=1&id=<?=$artigo['cod_usuario_profissional'];?>'>
                        <?php
                        $codigo_profissional = $artigo['cod_usuario_profissional'];
                        $profissional = new Usuario();
                        $nome_profissional = $profissional->busca_nome_usuario($codigo_profissional); 
                        echo $nome_profissional;
                        ?>
                      </a>
                    </div>
                <div class="description"><div class=texto_resumido><?= $pagina->resumo_texto($artigo['texto_artigo'], 189); ?></div></div>
                <br>
                <div class="ui label"><?=$artigo['tags']; ?></div>
                </div>
              </div>
            </div>
            <div class="extra content">
              <a class="left floated created" href=?pgs=modal_excluir_artigo&id=<?=$artigo["cod_artigo"];?>>
                <i class="remove icon"></i>
                
                <div class="ui horizontal label">Excluir</div>
              </a>
              <a class="left floated created" href=?pgs=atualiza_artigo&id=<?=$artigo['cod_artigo'];?> >
                <i class="write icon"></i>
                <div class="ui horizontal label">Atualizar</div>
              </a>
              <a href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo'];?> class="right floated created">Ver mais</a>
            </div>
          </div>
          


          <?php  }
          } ?>

          <br>
        </div>
      </div>
    </div>
          <div class="ui centered grid">
          <div class="large ui buttons">
            <a href='?pgs=meus_artigos_profissional&pg=<?=$anterior;?>' class="ui left attached button">Anterior</a>
            <a href='?pgs=meus_artigos_profissional&pg=<?=$proxima;?>' class="right attached ui button">Próxima</a>
          </div>
          </div>

  </div>

  <footer>
    <script type="./semantic/js/semantic.js"></script>
    <script type="./semantic/js/show-examples.js"></script>
  </footer>
</body>
