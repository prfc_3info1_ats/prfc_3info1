 <head>
  <link href="semantic/semantic.css" rel="stylesheet">
  <link href="css/CadastroUsuarioCSS.css" rel="stylesheet">
  <link href="js/CadastroUsuarioJS.js" rel="stylesheet">
  <title> Login </title>
</head>
<body>
  <div class="ui stackable centered grid">
    <div class="eight wide column">
      <h2 class="ui horizontal divider header">
        <div class="content">
          Entre na sua conta
        </div>
      </h2>
      <div class="eight wide column">
        <form class='ui form' method='post' action="./Controllers/processa_login.php">
          <div class="ui stacked secondary  segment">
            <div class="field"> <?=@$_GET['erro']; ?>
              <div class="two fields">
                <div class="field">
                  <label>E-mail</label>
                  <input type="text" name="email">
                </div>
                <div class="field">
                  <label>Senha</label>
                  <input type="password" name="senha">
                </div>
              </div>
              <button type="submit" name="" class="ui fluid large grey submit button">Entrar</button>
            </div>
          </form>

          <div class="ui right aligned ">
              <a href="?pgs=recuperacao_senha">Esqueceu a senha?</a> 
          </div>
 
        </div>
      </div>
    </body>
