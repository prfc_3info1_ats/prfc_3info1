<?php 
include_once "./Models/Usuario.php";
include_once "./Models/Login.php"; 

					  $logado = new Login();
						$esta_logado=$logado->taLogado();
						$codigo = $logado->retorna_codigo_usuario();
            $tipo = $logado->retorna_tipo();

            if ($esta_logado == false) {
              header('location:./?pgs=inicial');
            }
            if ($tipo == 2) {
              header('location:./?pgs=atualiza_profissional');
            }

            if ($tipo == 1) {
              header('location:./?pgs=inicial_usuario');
            }

			     	$usuario = new Usuario();
			     	$dados_usuario = $usuario->busca_dados($codigo);

			     	$dado = $dados_usuario[0];
            ?>

<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css/CadastroUsuarioCSS.css" rel="stylesheet">
    <link href="js/CadastroUsuarioJS.js" rel="stylesheet">
      <title> Atualizar Cadastro de Usuário </title>
</head>
<body>

<div class="ui stackable centered grid">
  <div class="eight wide column">
  <h2 class="ui horizontal divider header">
    <div class="content">
    Dados
    </div>
  </h2>
  <div class="eight wide column">
    <form class='ui form' method='post' action="./Controllers/processa_atualizacao_cadastro.php">
      <div class="ui stacked secondary  segment">
      <span><?=@htmlspecialchars($_GET['erro']);?></span>
        <div class="field">
            <label>Nome</label>
            <input type="text" name="nome_usuario" value = '<?=$dado['nome_usuario'];?>'>
        </div>
        <div class="field">
            <label>E-mail</label>
            <input type="text" name="email_usuario" value='<?=$dado['email_usuario'];?>' >
        </div>
        <div class="two fields">
              <div class="field">
                <label>Senha</label>
                  <input type="password" name="senha_usuario" value='<?=$dado['senha_usuario'];?>'>
              </div>
              <div class="field">
                <label>Confirmação de senha</label>
                  <input type="password" name="confsenha_usuario" value='<?=$dado["senha_usuario"];?>'>
              </div>

        </div>
        * A senha está criptografada, para alterá-la basta colocar sua nova senha e confirmar
        <div class="field">
            <label>Faça uma pergunta</label>
            <input type="text" name="rec_senha" value='<?=$dado['pergunta_chave'];?>' >
        </div>
        <div class="field">
            <label>Responda à pergunta</label>
            <input type="text" name="rec_senha_resposta" value="<?=$dado['resposta_chave'];?>" >
        </div>
        <button type="submit" name="Atualizar" value="atualizar" class="ui fluid large grey submit button">Atualizar</button>

      </div>

      <div class="ui error message"></div>

    </form>

  </div>
</div>

</body>
</html>
				
