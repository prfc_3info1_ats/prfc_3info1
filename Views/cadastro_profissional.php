<!DOCTYPE html>
 <head>
 	<meta charset="UTF-8">
    <link href="./semantic/semantic.css" rel="stylesheet">
    
      <title> Cadastro de Usuário </title>

  </head>
  <body>



          <div class="ui stackable centered grid">
            <div class="eight wide column">
            <h2 class="ui horizontal divider header">
              <div class="content">
              Cadastro de Profissional
              </div>
            </h2>
              <form class='ui form' method='post' action="./Controllers/processa_cadastro_profissional.php" enctype="multipart/form-data">
                <div class="ui stacked secondary segment">
                  <div class="field"><?= @htmlspecialchars($_GET['erro']); ?>
                    <div class="two fields">
                      <div class="field">
                        <label>CPF</label>
                          <input type="text" name="cpf" id="cpf" class="cpf" required>
                      </div>
                      <div class="field">
                        <label>Data de aniversário</label>
                          <input type="date" name="data" required>
                          <span>Você de deve ter mais de 21 anos</span>
                      </div>
                    </div>
                  <div class="fields">
                    <div class="three wide field">
                      <label>CRM</label>
                        <input type="text" name="crm" required>
                        </datalist>
                    </div>
                    <div class="seven wide field">
                      <label>Área de atuação</label>
                        <input type="text" name="area" required>
                    </div>
                    <div class="six wide field">
                      <label>Cidade em que atua</label>
                        <input type="text" name="cidade" required>
                    </div>
                  </div>
                  <div class="field">
                    <label>Telefone</label>
                      <input type="text" name="tel" id="tel" class="phone_with_ddd" required>
                  </div>
                  <div class="field">
                    <label>Foto</label>
                      <input type="file" class ="carregarfoto" name="foto_usuario" accept="image/*" >
                  </div>
                  <button type="submit" name="" class="ui fluid large grey submit button">Cadastrar</button>
                </div>

                <div class="ui error message"></div>

              </form>

  </div>
</div>

  <script type="text/javascript" src="https://code.jquery.com/jquery-latest.min.js"></script>
    <script type="text/javascript" src="./css_js/jQuery-Mask-Plugin-master/dist/jquery.mask.min.js"></script>
  <script> 
    $(document).ready(function(){
      $('.cpf').mask('000.000.000-00', {reverse: true});
      $('.phone_with_ddd').mask('(00) 0000-0000');

    });
  </script>

</body>
</html>