<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
<?php  $id_artigo = $_GET['id'];
 ?>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="remove icon"></i>
          Você já avaliou este artigo! Excluir avaliação?</div>
      <div class="actions">
          <form method="post" action="./Controllers/processa_excluir_avaliacao_artigo.php">
            <input type="hidden" name="id_artigo" value='<?=$id_artigo;?>'>
            <input type="submit" name="Exluir" class="ui left attached button">
          </form>
        </div>
      </div>
    </div>

</body>
</html>