<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
  <title></title>
 
  <link href="css_js/MeuCSS.css" rel="stylesheet">
  <link href="css_js/CadastroUsuarioJS.js" rel="stylesheet">  

</head>
<body>
  <?php
  include_once './Models/Profissional.php';
  include_once './Models/Usuario.php';


  $id = $_GET['id'];
  $profissional = new Profissional();
  $dados_profissional = $profissional->busca_dados($id);
  $dados = $dados_profissional[0];

$user=new Usuario();
$status_ativacao = $user->verifica_ativacao_usuario($id);



  ?>
  <div class="ui stackable grid">
    <div class="five wide column">
     
        <div class="ui fluid vertical large menu">

          <div class="ui fluid image">
            <div class="ui black right ribbon label">
              <i class="icon"></i>
              CRM: <?=$dados['crm']?>
            </div>
            <a>
              <img src='./imagens/<?=$dados["foto_perfil"]?>'>
            </a>
          </div>

          <div class="cabecalho-perfil">
            <div class="header"><h2><?=$dados['nome_usuario']?></h2></div>
            <div class="subtitle"><h5><?=$dados['area_atua']?></h5></div>
            <div class="subtitle"><h5><?=$dados['cidade_atua']?></h5></div>
            <div class="subtitle"><h5><?=$dados['data_nasc']?></h5></div>
            <div class="subtitle"><h5><?=$dados['telefone']?></h5></div>
          </div>
          <div class="ui divider"></div>
          <?php 
          include_once "./Models/Profissional.php";
          $avaliacao = new Profissional();
          $avaliacao_positiva = $avaliacao->busca_avaliacao_profissional_positiva($id);
          $avaliacao_negativa = $avaliacao->busca_avaliacao_profissional_negativa($id);
          ?>
          <form method="post" action="./Controllers/processa_avaliacao_profissional.php">
           <div class="avaliacao-profissional">
            <div class="ui buttons">
             <button class="ui button" name="avaliacao_profissional" value="não"><i class="thumbs down icon" title="Não gostei"></i><?=$avaliacao_negativa;?></button>
             <div class="or" data-text="ou"></div>
             <button class="ui grey button" name="avaliacao_profissional" value="sim" title="Gostei"><i class="thumbs up icon"></i><?=$avaliacao_positiva;?></button>
             <input type="hidden" name="id" value='<?=$_GET['id'];?>'>
           </div>
         </div>
       </form>
       <br>
      <?php if($status_ativacao == 1){ ?>
        <a class="item" href="?pgs=mensagem&id=<?=$id;?>" title="Enviar mensagem">Mensagem
          <i class="icon mail"></i>
        </a>
      <?php } else{
        echo "Profissional com conta desativada";
        } ?>
        </div>
    
</div>

<div class="ui eleven wide column">
  <?php
  include_once "artigos_profissional.php" ;
  ?>
</div>
</div>
</body>
</html>