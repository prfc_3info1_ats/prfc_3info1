<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        Sua senha foi alterada
      </div>
      <div class="actions">
          <a href="?pgs=form_login" class="ui button"> Ir para a página de login</a>
      </div>
    </div>
  </div>

</body>
</html>