<!DOCTYPE html>
<html>
<head>
	<title>AssisMed</title>
	<link href="./semantic/semantic.css" rel="stylesheet">
	
</head>
<body>

<h1 class="ui horizontal divider header">
    <i class="doctor icon"></i>
    Novos Médicos
  </h1>
<p></p>

<div class="ui stackable four column grid">
    <?php  
      include_once "./Models/Profissional.php";
      include_once "./Models/Artigo.php";
      include_once "./Models/Usuario.php";

      $user = new Login();
      $in_user=$user->taLogado();
      $tipo_user = $user->retorna_tipo();
        if ($in_user == false) {
            header('location:?pgs=inicial');
          }

      $limite_titulo = new Artigo();
      $profissionais = new Profissional();
      $ultimos_profissionais = $profissionais->busca_profissionais_limite(4);
      foreach ($ultimos_profissionais as $profissional) {

    ?>
        <div class="column">
          <div class="ui fluid card">
            <div class="ui fluid image">
              <div class="ui black ribbon label large">CRM:
                <?=$profissional['crm'] ;?>
              </div>
              <a class="image" href=?pgs=perfil_profissional&pg=1&id=<?=$profissional['cod_usuario'] ?>>
                <img src="./imagens/<?=$profissional['foto_perfil'] ?>">
              </a>
        
            </div>
            
            <div class="content">
              <a class="header" href=?pgs=perfil_profissional&pg=1&id=<?=$profissional['cod_usuario'] ?>><?=$profissional['nome_usuario'] ?></a>
              <div class="meta">
                <a><?=$profissional['area_atua'] ?></a>
              </div>
            </div>
          </div>
        </div>
    <?php
    }
    ?>
</div>

<h1 class="ui horizontal divider header">
    <i class="newspaper icon"></i>
    Novos Artigos
  </h1>
<p></p>








<div class="ui stackable four column grid">
 <?php  
      

      $Artigos = new Artigo();
      $ultimos_artigos = $Artigos->busca_artigos_limite(4);
      foreach ($ultimos_artigos as $artigo) {
      
      $cod_pessoa =$artigo['cod_usuario_profissional'];
      $informacao_pro = new Usuario();
      $info = $informacao_pro->busca_dados($cod_pessoa); 

      $area_pro = new Artigo();
      $area = $area_pro->buscar_area_profissional($cod_pessoa);
      
    ?>


    <div class="column">
      <div class="ui fluid card">
        
        <div class="ui fluid image">
          <div class="ui black ribbon label large">
          <?=$artigo['data_artigo'] ;?>
          </div>
          <a class="image" href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>>
            <img src="./imagens/<?=$artigo['imagem'] ;?>">
          </a>
        </div>
        <div class="content">
          <a class="header" href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>><?= $limite_titulo->resumo_texto($artigo['titulo_artigo'], 20); ?></a>
          <div class="meta">
            <a href=?pgs=perfil_profissional&pg=1&id=<?=$info[0]['cod_usuario']?>><?=$info[0]['nome_usuario']; ?></a>
          </div>
          <div class="meta">
            <a><?=$area['area_atua']; ?></a>
          </div>
        </div>
      </div>
    </div>
<?php
    }
?>
</div>







<h1 class="ui horizontal divider header">
    <i class="help icon"></i>
    Dúvidas sobre o site
  </h1>
<p></p>


<div class="ui stackable twelve centered grid">
  <div class="ui  four wide column">
  <div class="ui piled segment">
    <h2>As informações sobre saúde neste sistema são seguras?</h2>
    <p>Nosso sistema possui profissionais da área da saúde qualificados disponíveis para o auxílio à população. </p>
    <p>Ao se cadastrarem são exigidas informações relevantes (como o seu CPF, CRM, Cidade de atuação, etc.), quais são mostradas no perfil de cada médico justamente para que o usuário adquira esta confiabilidade.</p>
  </div>
  </div>
  <div class="eight wide column">
  <div class="ui piled segment">
    <h2>Como mando uma mensagem?</h2>
    <p>Para enviar uma mensagem a um profissional, deve-se entrar no perfil do mesmo e clicar no ícone de mensagem, qual se localiza na parte inferior esquerda, logo abaixo da foto de perfil.</p>
    <p>Após isso, você será direcionado a página de mensagens e poderá enviá-las para o respectivo médico.</p>
    <p>Caso queira visualizar suas mensagens, clique "Mensagens" (está no seu menu) e depois clique na mensagem que desejar ver.</p>
    <p>Você não poderá apagar as mensagens enviadas.</p>
    <p>Os profissionais não podem iniciar conversas com usuários comuns; eles apenas podem responder as suas dúvidas.</p>
  </div>
  </div>
  <div class="four wide column">
  <div class="ui piled segment">
    <h2>Formas de entrar em contato com um médico?</h2>
    <p> Existe um sistema de troca de mensagens, e uma parte de comentários ao final de cada artigo.</p>
    <p>Caso isto seja insuficiente, voçê pode entrar em contato com um profissional por meio de ligações; O número destes especialistas se encontra acessível em cada perfil.</p>
  </div>
  </div>
</div>

</body>
</html>
