<!DOCTYPE html>
<head>
  <meta charset="UTF-8">
    <link href="./semantic/semantic.css" rel="stylesheet">
    <link href="./css_js/CadastroUsuarioJS.js" rel="stylesheet">

      <title> Cadastro de Usuário </title>
  </head>
  <body>

<div class="ui stackable centered grid">
  <div class="eight wide column">
  <h2 class="ui horizontal divider header">
    <div class="content">
    Crie uma conta
    </div>
  </h2>
  <div class="eight wide column">
    <form class='ui form' method='post' action="./Controllers/processa_cadastro_usuario.php">
      <div class="ui stacked secondary segment">
        <div class="field"> <?= @$_GET['erro']; ?>
          <label>Nome</label>
            <input type="text" name="nome_usuario" autocomplete="off" required value="<?= @$_SESSION['dados_cadastro']['nome_usuario']; ?>">
        </div>
        <div class="field">
          <label>E-mail</label>
            <input type="text" name="email_usuario" autocomplete="off" required value="<?= @$_SESSION['dados_cadastro']['email_usuario']; ?>">
        </div>
        <div class="field">
            <div class="two fields">
              <div class="field">
                <label>Senha</label>
                  <input type="password" name="senha_usuario" autocomplete="off" required>
              </div>
              <div class="field">
                <label>Confirmação de senha</label>
                  <input type="password" name="confsenha_usuario" autocomplete="off" required>
              </div>
            </div>
             A senha deve conter letras e números, e possuir de 8 a 12 caracteres*
        </div>
        <div class="field">
          <label>Tipo de Usuário</label>
            <input list="browsers" name="tipo_user" autocomplete="off" required value="<?= @$_SESSION['dados_cadastro']['tipo_user']; ?>">
            <datalist id="browsers" >
                <option value="Leitor">
                <option value="Profissional">
            </datalist>
        </div>
        <div class="field">
          <label>Pergunta para recuperação de senha</label>
            <input type="text" name="rec_senha_pergunta" autocomplete="off" required value="<?= @$_SESSION['dados_cadastro']['rec_senha_pergunta']; ?>">
        </div>
        <div class="field">
          <label>Resposta para recuperação de senha</label>
            <input type="text" name="rec_senha_resposta" autocomplete="off" required value="<?= @$_SESSION['dados_cadastro']['rec_senha_resposta']; ?>">
        </div>
        <button type="submit" name="" class="ui fluid large grey submit button">Continuar</button>

      <div class="ui error message"></div>
    </form>

  </div>
</div>
</body>
</html>