<?php include_once'./Models/Profissional.php';
?>

<!DOCTYPE html>
<html>
<head>   
	<meta charset="UTF-8">
 
  	<title>Visualização Profissional</title>
     <link href="./semantic/semantic.css" rel="stylesheet">
     <link href="semantic/js/semantic.js">

</head>

<body>

<div class="ui centered grid">
  <p></p>
    <h2 class="ui horizontal divider header">
      <i class="doctor icon"></i>
      Profissionais
    </h2>
  <p></p>

  <div class="ui stackable middle aligned center aligned grid" >
    <?php 
    $profissionais = new Profissional();
    $busca_profissionais = $profissionais->busca_todos_profissionais();

    foreach ($busca_profissionais as $profissional) {?>
       
          <div class="ui stackable four wide column">
           <div class="ui stackable card">
             <div class="ui slide masked reveal image">
               <a href="?pgs=perfil_profissional">
                 <img src="./<?=$profissional['foto_perfil']?>">
               </a>
              </div>
              <div class="content">
                <a href="?pgs=perfil_profissional&pg=1&id=<?=$profissional['cod_usuario']?>" alt="<?=$profissional['nome_usuario']?>" class="header"><h2><?=$profissional['nome_usuario']?></h2></a>
                <div class="meta">
                  <div class="time"><?=$profissional['area_atua']?>
                  </div>
                </div> 
              </div>
            </div>
          </div>
       
  <?php } ?>
 
  </div>
 </div>
</body>
