<?php 
        include_once './Models/Comentario.php';
        include_once './Models/Login.php';
        include_once './Models/Usuario.php';
        include_once './Models/Administrador.php';

        #cadastrar um comentário
        if(isset( $_POST['texto_comentario'])){ 
          $texto_comentario = $_POST['texto_comentario'];
        } 

        $usuario = new Login();
        $cod_usuario = $usuario->retorna_codigo_usuario();
        $data_comentario = date('y/m/d');

        $id_artigo = $_GET['id'];


        if(isset($texto_comentario)){
          $comentario = new Comentario($texto_comentario);
          $comentario->cadastra_comentario($texto_comentario, $data_comentario,  $id_artigo, $cod_usuario);
        } 
        # fim cadastrar comentario

        #apagar um comentário
        if (isset($_POST['Excluir'])) {
          $adm = new Administrador();
          $adm_denuncia = $adm->apagar_denuncias($_POST['codigo_coment']);
   
          $apaga_comentario = new Comentario();
          $apaga_comentario->apagar_comentario($_POST['codigo_coment']);
        }
        # fim cadastrar comentario                

        $comentarios = new Comentario();
        $busca = $comentarios->mostra_comentario($id_artigo);

        if (empty($busca['cod_comentario']) AND !isset($busca['cod_comentario'])) {
          $coment = new Comentario();
          $comentario_puro = $coment->busca_apenas_comentario();
        }

?>
  
  <!-- ESPACO PARA COMENTARIOS -->
  <div class="ui stacked segment">
    <div class="ui comments">
      <h3 class="ui dividing header">Comentários</h3>
      <div class="comment">

        <?php foreach ($comentario_puro as $comentario):

          if (isset($comentario['cod_comentario_artigo'])) {
            $cod_user = $comentario['cod_usuario'];
            $data_coment = $comentario['data_comentario_artigo']; 
            $texto= $comentario['texto_comentario_artigo'];
          }
          
          $usuario = new Usuario();
          $consulta = $usuario->busca_nome_usuario($cod_user);
          ?>
          <!-- COMENTARIOS REALIZADOS -->

          <?php if ($id_artigo==$comentario['cod_artigo'] && $comentario['denunciado'] != 1): 
          //verifica se o comentario esta relacionado ao artigo ?> 
          
            <div class="content"> 
              <a class="author"> <?= $consulta ?></a>
              <div class="metadata">
                <span class="date"><?= $data_coment ?></span>
              </div>
              <div class="text">
                <p><?= $texto ?></p>
              </div>

              <!-- EXCLUIR O PROPRIO COMENTARIO -->
              <?php if ($cod_usuario == $cod_user):?>
              <form action=?pgs=modal_excluir_comentario method="post">
                  <input type="hidden" name="codigo_coment" value ="<?= $comentario['cod_comentario_artigo'] ?>" >
                  <input type="hidden" name="codigo_artigo" value ="<?= $id_artigo ?>" >
                  <input type="submit" class="ui negative basic button mini" name="Excluir" value="Excluir">
              </form>
              <?php endif; ?>

              <!-- DENUNCIAR -->
              <?php 
              $denuncia_busca = new Administrador();
              $busca_denuncia = $denuncia_busca->busca_denuncia($comentario['cod_comentario_artigo'], $comentario['cod_usuario'], $cod_usuario);
              if ($cod_usuario <> $cod_user) :
                  if(empty($busca_denuncia)){
              ?>
                    <form action="?pgs=modal_informa_denuncia" method="POST">
                      <input type="hidden" name="codigo_coment" value ="<?= $comentario['cod_comentario_artigo'] ?>" >
                      <input type="hidden" name="codigo_denunciador" value ="<?= $cod_usuario;  ?>" >
                      <input type="hidden" name="codigo_artigo" value ="<?= $id_artigo ;  ?>" >
                      <input type="hidden" name="tipo_comentario" value="comentario_puro">
                      <input type="submit" class="ui basic button mini" name="Denunciar" value="denunciar">
                    </form>
              <?php } endif; ?>

              <!-- RESPONDER COMENTARIOS -->

                <div class="resposta_box">
                  <a class="abre_resposta">Responder</a>
                  <form class="resposta ui reply form" action="./Controllers/processa_comentario.php" method="post">
                    <div class="ui secondary segment">
                      <input type="hidden" name="id_artigo" value="<?= $exibicao['cod_artigo'] ?>">
                      <input type="hidden" name="id_comentario" value="<?= $comentario['cod_comentario_artigo'] ?>">
                      <div class="ui fluid input action">
                        <input type="text" name="texto_comentario" placeholder="Insira aqui sua resposta...">
                        <button class="ui right floated button gray" type="submit" name="enviar">Enviar</button>
                      </div>
                    </div>
                  </form>
                </div>
                <p></p>
                <div class="ui divider"></div>
               <?php 

                  $resposta = new Comentario();
                  $verifica_resposta  = $resposta->busca_resposta_comentario($comentario['cod_comentario_artigo']);

                  foreach ($verifica_resposta as $resposta):

                    if (isset($resposta['cod_comentario_artigo'])) {
                      $cod_user = $resposta['cod_usuario'];
                      $data_coment = $resposta['data_comentario_artigo']; 
                      $texto = $resposta['texto_comentario_artigo'];
                    }

                    $usuario = new Usuario();
                    $consulta = $usuario->busca_nome_usuario($cod_user);

                    if ($resposta['denunciado'] != 1) {
                    ?>

                    <div class="content" style="margin-left: 7%"> 
                      
                      <a class="author"><?= $consulta ?></a>
                      <div class="metadata">
                        <span class="date"><?= $data_coment ?></span>
                      </div>
                      <div class="text">
                        <p><?= $texto ?></p>
                      </div>
                      
                      
                      <!-- EXCLUIR RESPOSTA -->                      
                      <?php if ($cod_usuario == $cod_user): ?> 
                      
                      <form action='?pgs=modal_excluir_comentario' method="post">
                        <input type="hidden" name="codigo_coment" value ="<?= $resposta['cod_comentario_artigo']; ?>" >
                        <input type="hidden" name="codigo_artigo" value ="<?= $id_artigo ?>" >
                        <input type="submit" name="Excluir" class="ui negative basic button mini" value="Excluir">
                      </form>
                      
                      <?php endif;?>
            <p></p>
                      <!-- DENUNCIAR RESPOSTA-->
                      <?php 
                        $denuncia_busca = new Administrador();
                        $busca_denuncia = $denuncia_busca->busca_denuncia($resposta['cod_comentario_artigo'], $resposta['cod_usuario'], $cod_usuario);
                        if ($cod_usuario <> $cod_user) :
                            if(empty($busca_denuncia)){
                        ?>
                      
                            <form action="?pgs=modal_informa_denuncia" method="POST">
                              <input type="hidden" name="codigo_coment" value ="<?= $resposta['cod_comentario_artigo'] ?>" >
                              <input type="hidden" name="codigo_denunciador" value ="<?= $cod_usuario;  ?>" >
                              <input type="hidden" name="codigo_artigo" value ="<?= $id_artigo ;  ?>" >
                              <input type="hidden" name="tipo_comentario" value="comentario_resposta">
                              <input type="submit" class="ui basic button mini" name="Denunciar" value="denunciar">
                            </form>
                            
                      <?php } endif; ?>
                      <div class="ui divider"></div>
                    </div>
                  <p></p>

                  <?php } 
                  endforeach; ?>

            </div> <!-- /.content -->

          <?php endif; ?>
        <?php endforeach; ?>

        <!-- FORMULARIO DE COMENTARIO PRINCIPAL -->
        <form class="ui reply form" action="./Controllers/processa_comentario.php" method="post">
          <div class="ui field">
            <div class="ui field">
              <textarea rows="2" name="texto_comentario" placeholder="Insira aqui seu comentário..."></textarea>
              <input type="hidden" name="id_artigo" value="<?=$exibicao['cod_artigo'];?>">
            </div>
            <div class="field">
              <input class="ui right floated button blue" type="submit" name="enviar">
              <br><br>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script> 
    $(document).ready(function(){
      $(".abre_resposta").click(function(){
        $(this).closest('.resposta_box').find('.resposta').slideToggle("slow");
      });
    });
  </script>