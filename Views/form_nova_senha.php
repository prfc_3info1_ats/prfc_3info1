<?php 
$email=unserialize($_GET['email']);
?>
<body>
  <div class="ui stackable centered grid">
    <div class="eight wide column">
      <h2 class="ui horizontal divider header">
        <div class="content">
          Recuperação de senha
        </div>
      </h2>
      <div class="ui eight wide column">
        <form class='ui form' method='post' action="./Controllers/processa_nova_senha.php">
          <div class="ui stacked secondary  segment">
            <div class="field">
                  <label>Digite sua nova senha</label>
                  <input type="password" name="senha">
                  <label>Digite sua nova senha novamente</label>
                  <input type="password" name="confirma_senha">
                  <input type="hidden" name="email" value="<?=$email?>">
            </div>
              <button type="submit" name="" class="ui fluid large grey submit button">Atualizar</button>
            </div>
          </form> 
        </div>
      </div>
    </body>
