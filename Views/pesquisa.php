<?php
  include_once'./Models/Login.php';

        $user = new Login();
        $in_user=$user->taLogado();
        $tipo_user = $user->retorna_tipo();
          if ($in_user == false) {
                  header('location:?pgs=inicial');
                  exit();
    }?>
<body>

<div class="ui container">

  <p></p>
     <h2 class="ui horizontal divider header">
       <i class="search icon"></i>
          Resultados
     </h2>
  <p></p>
  <div class="ui stackable four cards">



<?php
include_once './Models/Artigo.php';
include_once './Models/Usuario.php';
include_once './Models/Profissional.php';


  $busca=$_SESSION['pesquisa'];
  
$profissionais = $busca['profissionais'];
$artigos = $busca['artigos'];
$classe_artigo = new Artigo();

?>
<p></p>

<?php
foreach ($artigos as $artigo) {
            $codigo_profissional = $artigo['cod_usuario_profissional'];
            $profissional = new Profissional();
            $dado_profissional = $profissional->busca_dados($codigo_profissional); 

            $profissional_nome = new Usuario();
            $profissional_nome_2 = $profissional_nome->busca_nome_usuario($codigo_profissional); 
            $limite_titulo = new Artigo();
            

   ?>
      <div class="ui card">
        <div class="ui fluid image">
          <div class="ui black ribbon label large">
          <i class="newspaper icon"></i>
            Artigo
          </div>
          <a class="image" href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>>
            <img src="./imagens/<?=$artigo['imagem'] ?>">
          </a>
        </div>
        <div class="content">
          <a class="header" href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>><?=$limite_titulo->resumo_texto($artigo['titulo_artigo'], 20); ?></a>
          <div class="meta">
            <a  href=?pgs=perfil_profissional&pg=1&id=<?=$codigo_profissional?>><?= $profissional_nome_2; ?></a>

          <div class="meta">
            <a><?=$dado_profissional[0]['area_atua']; ?></a>
          </div>
        </div>
      </div>
    </div>

<?php  }  ?>

<?php
foreach ($profissionais as $profissional) { 
 ?>
        <div class="ui card">
            <div class="ui fluid image">
              <div class="ui black ribbon label large">
                <i class="doctor icon"></i>
                Profissional
              </div>
              <a class="image" href=?pgs=perfil_profissional&pg=1&id=<?=$profissional['cod_usuario'] ?>>
                <img src="./imagens/<?=$profissional['foto_perfil'] ?>">
              </a>
            </div>
            <div class="content">
              <a class="header" href=?pgs=perfil_profissional&pg=1&id=<?=$profissional['cod_usuario'] ?>><?=$profissional['nome_usuario'] ?></a>
              <div class="meta">
                <a><?=$profissional['area_atua'] ?></a>
              </div>
            </div>
          </div>
        </div>
    <?php
}
?>
</div>
</div>
</div>

<footer>
	<script type="./semantic/js/semantic.js"></script>
	<script type="./semantic/js/show-examples.js"></script>
</footer>
</body>