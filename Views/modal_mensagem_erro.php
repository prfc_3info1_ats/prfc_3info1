<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
<?php
?>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="Open Envelope Outline icon"></i>
        Você não pode mandar uma mensagem para este usuário.
      </div>
      <div class="actions">
        <div class="ui button">
          <a href='?pgs=mensagem'> Voltar para a página de mensagem</a>
        </div>
      </div>
    </div>
  </div>

</body>
</html>