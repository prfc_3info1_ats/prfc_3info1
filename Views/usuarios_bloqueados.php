<?php
include_once './Models/Administrador.php';
include_once './Models/Comentario.php';
include_once './Models/Usuario.php';
include_once './Models/Artigo.php';
include_once './Models/Login.php';
$administrador = new Login();
$logado = $administrador->taLogado();

$usuario_logado = new Login();
$usuario_adm = $usuario_logado->retorna_tipo();

if ($usuario_adm != 1 ) {
   header('location:./?pgs=inicial_usuario');
}

  $adm = new Administrador();
  $todas_denuncias = $adm->busca_denuncia_status();
  
  
?>
<h3 class="ui center aligned header">Usuários Bloqueados</h3>
<div class="ui container">
  <table class="ui center aligned celled table">
    <thead>
      <th>Usuários</th>
      <th>Artigo</th>
      <th>Comentários</th>
      <th>Desbloquear</th>
    </thead>
    
    <tbody>
      <?php
      if (isset($todas_denuncias)) {
  

  }
      foreach ($todas_denuncias as $denuncia):
                    $usuario = new Usuario();
                    $denunciado = $usuario->busca_nome_usuario($denuncia['cod_denunciado']);

                    $texto = new Comentario();
                    $texto_comentario = $texto->busca_texto_comentario($denuncia['cod_comentario']);

                    $tipo = new Usuario();
                    $tipo_user = $tipo->busca_tipo_usuario($denuncia['cod_denunciado']);
                 
                      if ($tipo_user[0]['tipo_usuario'] == 2) {
                        $tipo_user = "Usuário Profissional";
                      }else{
                        $tipo_user = "Usuário Leitor";
                      }

                    $artigo = new Artigo();
                    $dados_artigo = $artigo->busca_artigo($denuncia['cod_artigo']);
                    
      ?>
      <tr>
        <td>
          <h4 class="ui image header">
            <div class="content">
              <?= $denunciado?>
              <div class="sub header">
              <?= $tipo_user?>
            </div>
          </div>
        </td>
        <td>
          <h4 class="ui image header">
          <img src='./imagens/<?=$dados_artigo[0]['imagem'];?>' class="ui mini rounded image">
            <div class="content">
                <a href='?pgs=mostra_artigo&id=<?=$dados_artigo[0]['cod_artigo'];?>'>
                  <?=$dados_artigo[0]['titulo_artigo']?>
                </a> 
              </div>
            </td>
        <td>
          <?= $texto_comentario[0]['texto_comentario_artigo']?>
        </td>
        <td>
          <form  action='./Controllers/processa_exclui_denuncia.php' method="post">
            <input type="hidden" name="cod_comentario" value='<?= $denuncia['cod_comentario']?>'>
            <input type="hidden" name="cod_denunciado" value='<?= $denuncia['cod_denunciado']?>'>
            <button type="submit" class="ui button" name="Desbloquear" value='desbloquear'><i class="checkmark icon"></i>Desbloquear</button>
          </form>
        </td>
      </tr>
      <?php
      endforeach;
      ?>
    </tbody>
  </table>
</div>       