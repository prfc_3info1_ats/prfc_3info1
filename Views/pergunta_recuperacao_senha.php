<head>
		<link href="../semantic/semantic.css" rel="stylesheet">
		<link href="../css/CadastroUsuarioCSS.css" rel="stylesheet">
		<link href="../js/CadastroUsuarioJS.js" rel="stylesheet">
		<title> Recuperação de senha </title>
	</head>
	<body>
	<?php 
	$busca=unserialize($_GET['pergunta']);
	?>
		<div class="ui stackable centered grid">
			<div class="eight wide column">
				<h2 class="ui horizontal divider header">
					<div class="content">
						Recuperação de senha
					</div>
				</h2>
				<div class="eight wide column">
					<form class='ui form' method='post' action="./Controllers/processa_recuperacao_senha_resposta.php">
						<div class="ui stacked secondary  segment">
							<div class="field">
								<label>Pergunta: <?= $busca['pergunta_chave'] ?></label>
								<input type="hidden" name="resposta_esperada" value='<?= $busca["resposta_chave"]; ?>'>
								<input type="hidden" name="email" value='<?=$busca["email"];?>'>
								<label>Resposta:</label>
								<input type="text" name="resposta">
							</div>
							<input type="submit" name="" class="ui fluid large grey submit button">
						</div>
					</form> 
				</div>
			</div>
		</body>