<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
<?php
?>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="Open Envelope Outline icon"></i>
        Você tem certeza de que deseja denunciar este comentário?
      </div>
      <div class="actions">
        <div class="ui cancel button">
          <a href="?pgs=inicial_usuario">Não</a>
        </div>
        <div class="ui approve button">
          <a href="./Controllers/processa_denuncia.php"> Sim</a>
        </div>
      </div>
    </div>
  </div>

</body>
