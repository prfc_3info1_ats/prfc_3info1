<?php 
include_once "./Models/Usuario.php";
include_once "./Models/Profissional.php";
include_once "./Models/Login.php"; 

			      $user = new Login();
			      $in_user=$user->taLogado();
			      $tipo_user = $user->retorna_tipo();
			        if ($in_user == false) {
			            header('location:?pgs=inicial');
			        }
			        if ($tipo_user == 3 && $tipo_user = 1){
			            header('location:?pgs=inicial_usuario');
			        }

					$logado = new Login();
					$esta_logado=$logado->taLogado();
					$codigo = $logado->retorna_codigo_usuario();
           
			     	$usuario = new Usuario();
			     	$dados_usuario = $usuario->busca_dados($codigo);

			     	$profissional = new Profissional();
			     	$dados_profissional = $profissional->busca_dados($codigo);




?>



<head>
<meta charset="UTF-8">
    <link href="semantic/semantic.css" rel="stylesheet">
    <link href="css/CadastroUsuarioCSS.css" rel="stylesheet">
    <link href="js/CadastroUsuarioJS.js" rel="stylesheet">
      <title> Atualizar Cadastro de Usuário </title>
</head>
<body>


		<div class="ui stackable centered grid">
		  <div class="eight wide column">
		  <h2 class="ui horizontal divider header">
		    <div class="content">
    		Dados
		    </div>
		  </h2>

		<?php

	    $dado= $dados_usuario[0]; 
	    $dado_complementar = $dados_profissional[0];
		$usuario = new Login();
		$cod_usuario = $usuario->retorna_codigo_usuario();

		$user = new Login();
		$in_user=$user->taLogado();
		$tipo_user = $user->retorna_tipo();
			if ($in_user == true) {

				if ($tipo_user == 2 ){?>
				<div class="eight wide column">
					    <form class="ui form" method="post" action="./Controllers/processa_atualizacao_cadastro.php" enctype="multipart/form-data">
					      <div class="ui stacked secondary  segment">
					      <span><?php

					      if (@$_GET['erro1'] <> 'Array') {
					      	echo @$_GET['erro1'];
					      } echo '<br>';
					      if (@$_GET['erro2'] <> '') {
					       	echo @$_GET['erro2'];
					       }

					        ?> </span>
					        <div class="field">
				              <label>Nome</label>
					            <input type="text" name="nome_usuario" value = '<?=$dado["nome_usuario"];?>'>
					        </div>
					        <div class="field">
					          <label>E-mail</label>
					            <input type="text" name="email_usuario" value='<?=$dado['email_usuario'];?>'>
					        </div>
					        <div class="two fields">
				              <div class="field">
				                <label>Senha</label>
				                  <input type="password" name="senha_usuario" value='<?=$dado['senha_usuario'];?>'>
				              </div>
				              <div class="field">
				                <label>Confirmação de senha</label>
				                  <input type="password" name="confsenha_usuario" value='<?=$dado['senha_usuario'];?>'>
				              </div>
				            </div>
				                    * A senha está criptografada, para alterá-la basta colocar sua nova senha e confirmar

					        <div class="field">
					          <label>Faça uma pergunta</label>
					            <input type="text" name="rec_senha" value='<?=$dado['pergunta_chave'];?>'>
					        </div>
					        <div class="field">
					          <label>Responda à pergunta</label>
					            <input type="text" name="rec_senha_resposta" value="<?=$dado['resposta_chave'];?>">
					        </div>
					        <div class="field">
					        	<div class="two fields">
							        <div class="field">
							          <label>CPF</label>
							            <span><?=$dado_complementar['cpf'];?></span>
							            <input type="hidden" name="cpf" value="<?=$dado_complementar['cpf'];?>">
							        </div>	        
							        <div class="field">
							          <label>Data de nascimento</label>
							            <input type="date" name="data" value="<?=$dado_complementar['data_nasc'];?>">
							        </div>
							    </div>
							</div>
							<div class="fields">
						        <div class="three wide field">
						          <label>CRM</label>
						             <span><?=$dado_complementar['crm'];?></span>
						        </div>
						        <div class="seven wide field">
						          <label>Área de atuação</label>
						            <input type="text" name="area" value="<?=$dado_complementar['area_atua'];?>">
						        </div>	        	        
						        <div class="six wide field">
						          <label>Cidade em que atua</label>
						            <input type="text" name="cidade" value="<?=$dado_complementar['cidade_atua'];?>">
						        </div>
						    </div>
					        <div class="field">
					          <label>Telefone</label>
					            <input type="text" name="tel" value="<?=$dado_complementar['telefone'];?>">
					        </div>
					        <div class="field">
					          <label>Foto de perfil</label>
					            <input type="file" name="foto_usuario_novo" accept="image/*" value='<?=$dado_complementar['foto_perfil'];?>'>
               					 <input type='hidden' name="foto_usuario"  value='<?=$dado_complementar['foto_perfil'];?>' >
					        </div>


					        <button type="submit" name="Atualizar" value="atualizar" class="ui fluid large grey submit button">Atualizar</button>

					      </div>
					    </form>
				</div>
				<?php 
				}else{
				echo "Usuário não pode acessar a página";
					}
				}
		?>

	</div>
	</div>
</body>