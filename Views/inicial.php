<!DOCTYPE html>
<head>
	<title>AssisMed</title>
	<link href="./semantic/semantic.css" rel="stylesheet">
	
</head>
<body>

<h1 class="ui horizontal divider header">
    <i class="users icon"></i>
    Sobre Nós
  </h1>
<p></p>

<div class="ui stackable twelve centered grid">
  <div class="seven wide column">
  <div class="ui piled segment">
    <h2>Quem Somos?</h2>
    <p>Um grupo de estudantes do terceiro ano do curso de Informática do Instituto Federal Catarinense dde Ciência e Tecnologia - Campus Araquari.</p>
    <p></p>
    <p><strong>Alunas:</strong></p>
    <p>Aline J. Hack</p>
    <p>Sílvia L. Demarchi</p>
    <p>Thamires R. Vessani</p>
  </div>
  </div>
  <div class="seven wide column">
  <div class="ui piled segment">
    <h2>Qual é o nosso objetivo?</h2>
    <p>Proporcionar um diálogo e a troca de informações (por meio de mensagens) entre profissionais da área da saúde e a população.</p>
    
    <p>A população costuma buscar informações relacionadas à saúde na internet, e muitas vezes as mesmas não são muito seguras. Assim, atráves do nosso site você poderá conversar com um médico e sanar dúvidas a respeito do campo da saude.</p>
    <p>Aqui você poderá encontrar vários artigos escritos por especialistas.</p>
    <p><strong>Junte-se a nós!</strong></p>
  </div>
  </div>
</div>
</body>
</html>


























</body>
</html>
