<?php
include_once './Models/Login.php'; 
?>


<!DOCTYPE html>
<html>
	<head>
	 	<meta charset="UTF-8">
	    <link href="./semantic/semantic.css" rel="stylesheet">
	    
	    <title> Cadastro de artigo </title>
	</head>
<body>

<div class="ui stackable centered grid">
  <div class="ui eight wide column">
  <h2 class="ui horizontal divider header">
    <div class="content">
      Crie um artigo
    </div>
    </h2>

<?php
@$usuario = new Login();
@$cod_usuario = $usuario->retorna_codigo_usuario();

@$user = new Login();
@$in_user=$user->taLogado();
@$tipo_user = $user->retorna_tipo();
	if ($in_user == false){ 
    header('location:?pgs=inicial');
} 

		if ($tipo_user == 2 ) {?>
			<form class="ui form" method="POST" action="./Controllers/processa_artigo.php" enctype="multipart/form-data">
    		<div class="ui stacked secondary segment">
            <div class="field">
              <label>Título</label>
                <input type="text" name="titulo" required>
            </div>
            <div class="field">
              <label>Texto</label>
                <textarea name="text_artigo" rows="10" cols="40" placeholder="Escreva o seu texto aqui..." required></textarea>
            </div>
            <div class="field">
             <label>Foto</label>
                <input type="file" class ="fotoartigo" name="fotoartigo" placeholder="Foto" accept="image/*" >
            </div>
            <div class="field">
             <label>Palavras chave</label>
                <input type="text" name="tags" required>
            </div>
            <input type="hidden" name="codigo_usuario" value='<?=$cod_usuario; ?>'>
            <button type="submit" name="" class="ui fluid large grey submit button">Publicar</button>
             <div class="ui error message"></div>
    			</form>
		<?php }else{
			echo "Usuário não pode acessar a página";
		}
		
	
 

?>
   
  </div>
</div>




</body>
</html>