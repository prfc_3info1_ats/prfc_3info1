<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="./semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="thumbs up icon"></i>
        Sua denúncia foi excluída! Agora o usuário não está mais bloqueado!
      </div>
      <div class="actions">
          <a href="?pgs=usuarios_bloqueados" class="ui button">Voltar para a página de usuários bloqueados</a>
      </div>
    </div>
  </div>

</body>
