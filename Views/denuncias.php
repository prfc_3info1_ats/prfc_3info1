<?php
include_once './Models/Administrador.php';
include_once './Models/Comentario.php';
include_once './Models/Usuario.php';
include_once './Models/Artigo.php';
include_once './Models/Login.php';

$administrador = new Login();
$logado = $administrador->taLogado();

$usuario_logado = new Login();
$usuario_adm = $usuario_logado->retorna_tipo();

if ($usuario_adm != 1 ) {
   header('location:./?pgs=inicial_usuario');
}


	$adm = new Administrador();
	$todas_denuncias = $adm->busca_todas_denuncias();
  
  

?>
<h3 class="ui center aligned header">Denúncias</h3>

<?php
  if (empty($todas_denuncias[0]['status_denuncia'] == 0)) {
    ?>
    <div class="ui container">
      
    <?php
    echo "você não tem nenhuma denuncia!";
    ?>
   </div>
    <?php
    
  }

?>
<div class="ui container">
  <table class="ui center aligned celled table">
    <thead>
      <th>Comentários</th>
      <th>Artigos</th>
      <th>Usuário Denunciado</th>
      <th>Denúncia</th>
    </thead>
    <tbody>
    <?php
    foreach ($todas_denuncias as $denuncia):
      $verifica = new Administrador();
      $verifica_denuncia = $verifica->conta_denuncia($denuncia['cod_comentario']);
      

      if ($verifica_denuncia[0]['count(cod_denuncia)']>= 5 && $denuncia['status_denuncia'] != 1) {
        
                    $artigo = new Artigo();
                    $dados_artigo = $artigo->busca_artigo($denuncia['cod_artigo']);
                    
                    $usuario = new Usuario();
                    $nome_pro = $usuario->busca_nome_usuario($dados_artigo[0]['cod_usuario_profissional']);

                    $usuario2 = new Usuario();
                    $denunciado = $usuario2->busca_nome_usuario($denuncia['cod_denunciado']);

                   

                    $texto = new Comentario();
                    $texto_comentario = $texto->busca_texto_comentario($denuncia['cod_comentario']);

                    ?>

                      <tr>
                        <td>
                          <?=$texto_comentario[0]['texto_comentario_artigo'];?>
                        </td>
                        <td>
                          <h4 class="ui image header">
                            <img src='./imagens/<?=$dados_artigo[0]['imagem'];?>' class="ui mini rounded image">
                            <div class="content">
                              <a href='?pgs=mostra_artigo&id=<?=$dados_artigo[0]['cod_artigo'];?>'>
                                <?=$dados_artigo[0]['titulo_artigo']?>
                              </a>
                              <div class="sub header"><?=$nome_pro?>
                            </div>
                          </div>
                        </td>
                        
                        <td>
                         <?=$denunciado;?>
                        </td>

                        <td>
                            <form  action='./Controllers/processa_confirma_denuncia.php' method="post">
                                <input type="hidden" name="cod_comentario" value='<?= $denuncia['cod_comentario']?>'>
                                <input type="hidden" name="cod_denunciado" value='<?= $denuncia['cod_denunciado']?>'>
                               <button type="submit" class="ui button" name="Confirmar Denúncia" value='confirmar'><i class="checkmark icon"></i>Confirmar</button>
                            </form>
                        </td>
                        

                      </tr>
        

     <?php
     }

     endforeach;
     ?>
    
    </tbody>
  </table>
</div>       

<br><br>