<?php 
        include_once "./Models/Mensagem.php";
        include_once "./Models/Login.php";
        include_once "./Models/Profissional.php";
        include_once "./Models/Usuario.php";


              $codigo_usuario = new Login();
              $codigo_remetente = $codigo_usuario->retorna_codigo_usuario();

              $login = new Login();
              $tipo_usuario = $login->retorna_tipo($codigo_remetente);

              $dados = new Usuario();
              $dados_usuario = $dados->busca_dados($codigo_remetente);

             

               
?>
<!--se eu sou profissional verei um leitor -->  
<div class="ui stackable  grid">
<?php
if ($tipo_usuario ==2) { 
              $mensagem = new Mensagem();
              $contatos = $mensagem->busca_todos_cod_re_contatos_mensagens($codigo_remetente);

              if (!empty($contatos)) {
               
?>
                <div class="ui stackable seven wide column">
                <div class="ui stackable segment">
                  <div class="ui relaxed divided list">  
                    <?php 
                      foreach ($contatos as $contato){
                        $informacoes = new Usuario();
                        $informacoes_contatos = $informacoes->busca_dados($contato['cod_usuario_remetente']);
                       
                                  foreach ($informacoes_contatos as $informacao_contato){   
                                  ?>
                                    <div class="item">
                                      <img class="ui avatar smoll image" src='./imagens/user.png'>
                                      <div class="content">
                                        <a href='?pgs=mensagem&id=<?= $informacao_contato["cod_usuario"]; ?>' class="header">
                                        <?= $informacao_contato['nome_usuario']; ?></a>
                                        <div class="description">
                                          Usuário Leitor
                                        </div>
                                      </div>
                                    </div>
                   <?php }}?>
                  </div><!-- end ui relaxed divided list -->  
                  </div> <!-- end ui segment -->  
                </div><!-- end seven wide column -->                 
<?php         }else{
?>
                <div class="ui stackable seven wide column">
                  <div class="ui stackable segment">
                    <div class="ui relaxed divided list">
                      <div class="description">
                        Você não tem nenhuma mensagem no seu histórico.
                      </div>
                    </div>
                  </div>
                </div>

      <?php
      }
      ?>                      
            
<!--se eu sou leitor verei um profissional -->              
<?php
}elseif ($tipo_usuario==3) {
              $mensagem = new Mensagem();
              $contatos = $mensagem->busca_todos_cod_des_contatos_mensagens($codigo_remetente);
              
              if (!empty($contatos)) {
?>

                <div class="ui stackable seven wide column">
                <div class="ui stackable segment">
                  <div class="ui relaxed divided list">
                    
                    <?php 
                      foreach ($contatos as $contato){
                        $informacoes = new Usuario();
                        $informacoes_contatos = $informacoes->busca_dados($contato['cod_usuario_destinatario']);
                        
                                  foreach ($informacoes_contatos as $informacao_contato){
                                      
                                      $dados_pro = new Profissional();
                                      $dados_profissional = $dados_pro->busca_dados($contato['cod_usuario_destinatario']);
                                     
                                  ?>

                                    <div class="item">
                                      <img class="ui avatar smoll image" src='./imagens/<?= $dados_profissional[0]['foto_perfil']; ?>'>
                                      <div class="content">
                                        <a href='?pgs=mensagem&id=<?= $dados_profissional[0]["cod_usuario"]; ?>' class="header">
                                          <?= $informacao_contato['nome_usuario']; ?></a>
                                        <div class="description">
                                          <?= $dados_profissional[0]['area_atua']; ?>
                                        </div>
                                      </div>
                                    </div>
                   <?php }}?>
                  </div><!-- end ui relaxed divided list -->    
                  </div><!-- end ui segment --> 
                </div><!-- end seven wide column --> 
<?php         }else{
?>
                <div class="ui stackable seven wide column">
                  <div class="ui stackable  segment">
                    <div class="ui relaxed divided list">
                      <div class="description">
                        Você não tem nenhuma mensagem no seu histórico.
                      </div>
                    </div>
                  </div>
                </div>

      <?php
      }
      ?>          
            
          
<?php } ?>  
</div>
