<!DOCTYPE html>
<html>
<head>
  <title></title>

  <link href="./css_js/MeuCSS.css" rel="stylesheet" type="text/css" /> 
</head>

<body>

<div class="ui container">

<div class="ui stackable centered grid">
<div class="ui stackable twelve wide column">
<p></p>
  <h2 class="ui horizontal divider header">
    <i class="thumbs up icon"></i>
    Minhas Curtidas
  </h2>
<p></p>

          <?php
            include_once "./Models/Artigo.php"; 
            include_once "./Models/Usuario.php";
            include_once "./Models/Login.php";
            include_once "./Models/Profissional.php";

            $limite_titulo = new Artigo();
            
            $logado = new Login();
			$esta_logado=$logado->taLogado();
						
            if ($esta_logado == false) {
              header('location:./?pgs=inicial');
            }
            
            $login = new Login();
            $meu_codigo = $login->retorna_codigo_usuario();
            
            $dados = new Artigo();
            $curtidas_dados_artigos = $dados->busca_curtidas($meu_codigo);
            
            $pagina = new Artigo();
   
            if (!empty($curtidas_dados_artigos)) {
            	 foreach ($curtidas_dados_artigos as $dado) {
            	 	
            	 	$foto_pro = new Artigo();
            	 	$foto = $foto_pro->buscar_foto_profissional($dado['cod_usuario_profissional']);
            	 	
            	 	?>
		              <div class="ui stackable card meu-card">
		              	
		                <div class="content">
		                <div class="ui grid centered">
		                	<div class="four wide column">
		                <div class="ui medium rounded image">
						  <img src='./imagens/<?= $dado["imagem"];?>' class="visible content">
						</div>

						</div>
						<div class="twelve wide column">
		                  <div class="right floated date"><?=$dado['data_artigo'] ?></div>
		                  <h3>
		                  <div class="header">
			                  <a href='?pgs=mostra_artigo&pg=1&id=<?=$dado['cod_artigo'];?>'>
			                  	<?=$limite_titulo->resumo_texto($dado['titulo_artigo'], 20); ?>
			                  </a>
		                  </div>
		                  </h3>
		                  <div class="meta">
		                  <h4>
		                    <a class="group" href='?pgs=perfil_profissional&pg=1&id=<?=$dado['cod_usuario_profissional'];?>'>
		                      <?php
		                        $codigo_profissional = $dado['cod_usuario_profissional'];
		                        $profissional = new Usuario();
		                        $nome_profissional = $profissional->busca_nome_usuario($codigo_profissional); 
		                        echo $nome_profissional;
		                      ?>
		                    </a>
		                    <h4>
		                  </div>
		                  <div class="description"><div class=texto_resumido><?= $pagina->resumo_texto($dado['texto_artigo'], 189); ?></div></div>
		                  <br>
		                  <div class="ui label right"><?=$dado['tags'] ?></div>
		                </div>
		                </div>
		                </div>
		                <div class="extra content">
		              		<?php 
					            $avaliacao = new Artigo();
					            $avaliacao_negativa = $avaliacao->busca_avaliacao_artigo(0, $dado['cod_artigo']);
					        ?>
					        
					        <form method="post" action="./Controllers/processa_excluir_avaliacao_artigo.php">
			                  <a href='./Controllers/processa_excluir_avaliacao_artigo.php?&id_artigo=<?=$dado["cod_artigo"]; ?>' value="<?=$dado["cod_artigo"]; ?>" class="left floated created">
			                    	<i class="thumbs outline down icon large"></i>
			                  </a>
			                  <input type="hidden" name="id_artigo" value=<?=$dado['cod_artigo']; ?>>
			                </form> 
							
			                  <a href=?pgs=mostra_artigo&id=<?=$dado['cod_artigo']?> class="right floated created">Ver mais</a>
		                </div>

		              </div>            	 	


            	 	<?php

            	 }

            }
              
            $curtidas_profissional = new Profissional();
            $curtidas_dados_profissional = $curtidas_profissional->busca_curtidas_profissional($meu_codigo);
   
          if (!empty($curtidas_dados_profissional)) {
            	 foreach ($curtidas_dados_profissional as $dado_pro) {
            	 	
            	 	
            	 	$informacoes = new Profissional();
            	 	$info_pro = $informacoes->busca_dados($dado_pro['cod_usuario_avaliado']);
            	 	$info_usuario = new Usuario();
            	 	$info_user = $info_usuario->busca_dados($dado_pro['cod_usuario_avaliado']);
            	 	
            	 	?>
		              <div class="ui card meu-card">
		              	
		                <div class="content">
		                <div class="ui grid centered">
		                	<div class="four wide column">
		                <div class="ui small circular rotate left image">
						  <img src='./imagens/<?= $info_pro[0]["foto_perfil"];?>' class="visible content">
						</div>

						</div>
							<div class="twelve wide column">
				                <div class="right floated date"><?=$info_pro[0]['crm'] ?></div>
				                <h3>
				                  <div class="header">
					                  <a href='?pgs=perfil_profissional&pg=1&id=<?=$info_pro[0]['cod_usuario'];?>'>
					                  	<?=$info_user[0]['nome_usuario']; ?>
					                  </a>
				                  </div>
				                </h3>
					                  <div class="meta">
						                  <h4>
						                    <a class="group">
						                      <?= $info_pro[0]["area_atua"];?>
						                    </a>
						                  </h4>
					                  </div>
				                  </div>
			                </div>
		                </div>
		                <div class="extra content">
			                  <a href=?pgs=perfil_profissional&pg=1&id=<?=$info_pro[0]['cod_usuario']?> class="right floated created">Ver Perfil</a>
		                </div>

		              </div>            	 	


            	 	<?php

            	 }

            }

            if (empty($curtidas_dados_profissional) && empty($curtidas_dados_artigos)){
            ?> 
            <div class="ui middle container">
            	<div class="ui compact aligned center segment">
                	<p>Você não tem nenhuma curtida .</p>
            	</div>
            </div>
            <?php
            }

          ?>


</div>
</div>
</div>

        
</div>

<footer>
  <script type="./semantic/js/semantic.js"></script>
  <script type="./semantic/js/show-examples.js"></script>
</footer>
</body>
</html>