<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="./semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="thumbs up icon"></i>
        Seus dados foram atualizados
      </div>
      <div class="actions">
          <a href="?pgs=inicial_usuario" class="ui button"> Voltar</a>
      </div>
    </div>
  </div>

</body>
</html>