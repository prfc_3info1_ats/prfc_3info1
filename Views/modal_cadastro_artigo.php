<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
    <link href="css_js/MeuCSS.css" rel="stylesheet">
    <link href="./semantic/semantic.css" rel="stylesheet">
    <link href="css_js/MeuJS.js" rel="stylesheet">
    <title></title>

</head>
<body>
   <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="thumbs up icon"></i>
        O artigo foi cadastrado!
      </div>
      <div class="actions">
          <a href="?pgs=meus_artigos_profissional&pg=1" class="ui button"> Continuar</a>
      </div>
    </div>
  </div>

</body>
</html>