<!DOCTYPE html>
<html>
<head>   
  <meta charset="UTF-8">
  <link href="semantic/semantic.css" rel="stylesheet">
  <link href="css_js/MeuCSS.css" rel="stylesheet">
  <link href="css_js/MeuJS.js" rel="stylesheet">
  <script src="./semantic/js/modal.init.js" type="text/javascript"></script>
  <script src="./semantic/js/modal.js" type="text/javascript"></script>
</head>
<body>

  <div class="meu-modal">
    <div class="active ui modal">
      <div class="ui icon header">
        <i class="remove icon"></i>
        Deseja mesmo excluir sua conta?
      </div>
      <div class="actions">
        <div class="ui cancel button">
          <a href="?pgs=inicial_usuario">Não</a>
        </div>
        <div class="ui approve button">
          <a href="./Controllers/apagar_usuario.php"> Sim</a>
        </div>
      </div>
    </div>
  </div>
</div>



</body>
</html>
