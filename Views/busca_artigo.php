
<body>

<div class="ui container">
<p></p>
  <h2 class="ui horizontal divider header">
    <i class="newspaper icon"></i>
    Artigos
  </h2>
<p></p>
<div class="ui stackable four cards">

<?php
include_once "./Models/Artigo.php"; 
include_once "./Models/Usuario.php";

$limite_titulo = new Artigo();
$pg_atual = $_GET['pg'];
$pagina = new Artigo();
$quantidade = 8;
$artigos_paginacao = $pagina->paginacao_artigo($pg_atual, $quantidade);

$n_pagina = new Artigo();
      $limite = $n_pagina->quantidade_paginas_artigo($quantidade);
     
      $anterior = $pg_atual -1;
      $proxima = $pg_atual + 1;
              if ($pg_atual=1) {
                $anterior = $pg_atual;
                }

if ($_GET['pg']>$limite) {
          ?>
          <p></p>
          <a href='?pgs=busca_artigo&pg=<?=$anterior;?>' class="">Voltar para a primeira página</a>
          <?php
          exit();
        }

foreach ($artigos_paginacao as $artigo) {?>


  <div class="ui card">
    
      <a class="ui masked reveal image" href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>>
        <img src="./imagens/<?=$artigo['imagem'] ?>">
      </a>
    
    <div class="content">
      <div class="header">
        <a href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?>>
          <?=$limite_titulo->resumo_texto($artigo['titulo_artigo'], 20) ?>
        </a>
      </div>
      <div class="meta">
        <a href=?pgs=perfil_profissional&pg=1&id=<?=$artigo['cod_usuario_profissional']?> class="group">
          <?php
            $codigo_profissional = $artigo['cod_usuario_profissional'];
            $profissional = new Usuario();
            $nome_profissional = $profissional->busca_nome_usuario($codigo_profissional); 
            echo $nome_profissional;
          ?>
        </a>
      </div>
      <div class="description"><div class=texto_resumido><?= $pagina->resumo_texto($artigo['texto_artigo'], 200); ?></div></div>
      <br>
      <div class="ui label right"><?=$artigo['tags'] ?></div>
    </div>
    <div class="extra content">
      <a href=?pgs=mostra_artigo&id=<?=$artigo['cod_artigo']?> class="right floated created">Ver mais</a>
      
    </div>
  </div>



<?php  }?>

<br>
</div>
<p></p>
          <div class="ui centered grid">
          <div class="large ui buttons">
            <a href='?pgs=busca_artigo&pg=<?=$anterior;?>' class="ui left attached button">Anterior</a>
            <a href='?pgs=busca_artigo&pg=<?=$proxima;?>' class="right attached ui button" tabindex="3">Próxima</a>
          </div>
          </div>
</div>

<footer>
	<script type="./semantic/js/semantic.js"></script>
	<script type="./semantic/js/show-examples.js"></script>
</footer>
</body>
