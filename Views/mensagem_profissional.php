<?php 
        include_once "./Models/Mensagem.php";
        include_once "./Models/Login.php";
        include_once "./Models/Profissional.php";
        include_once "./Models/Usuario.php";


              $codigo_usuario = new Login();
              $codigo_remetente = $codigo_usuario->retorna_codigo_usuario();

              $login = new Login();
              $tipo_usuario = $login->retorna_tipo($codigo_remetente);

              $dados = new Usuario();
              $dados_usuario = $dados->busca_dados($codigo_remetente);
              
?>
<html>
<head>
  <title></title>
  <link href="semantic/semantic.css" rel="stylesheet">
  <link href="./css_js/MeuCSS.css" rel="stylesheet">
</head>
<body>


<!--parte do menu  --> 
<!--se eu sou profissional verei um leitor -->  
<div class="ui stackable grid">
<?php

if($tipo_usuario==3) {
              $mensagem = new Mensagem();
              $contatos = $mensagem->busca_todos_cod_des_contatos_mensagens($codigo_remetente);
              
              if (!empty($contatos)) {
?>

                <div class="ui seven wide column">
                <div class="ui segment">
                  <div class="ui relaxed divided list">
                    
                    <?php 
                      foreach ($contatos as $contato){
                        $informacoes = new Usuario();
                        $informacoes_contatos = $informacoes->busca_dados($contato['cod_usuario_destinatario']);
                        
                                  foreach ($informacoes_contatos as $informacao_contato){
                                      
                                      $dados_pro = new Profissional();
                                      $dados_profissional = $dados_pro->busca_dados($contato['cod_usuario_destinatario']);
                                     
                                  ?>

                                    <div class="item">
                                      <img class="ui avatar smoll image" src='./imagens/<?= $dados_profissional[0]['foto_perfil']; ?>'>
                                      <div class="content">
                                        <a href='?pgs=mensagem&id=<?= $dados_profissional[0]["cod_usuario"]; ?>' class="header">
                                          <?= $informacao_contato['nome_usuario']; ?></a>
                                        <div class="description">
                                          <?= $dados_profissional[0]['area_atua']; ?>
                                        </div>
                                      </div>
                                    </div>
                   <?php }}?>
                  </div><!-- end ui relaxed divided list -->    
                  </div><!-- end ui segment --> 
                </div><!-- end seven wide column --> 
<?php         }else{
?>
                <div class="ui seven wide column">
                  <div class="ui segment">
                    <div class="ui relaxed divided list">
                      <div class="description">
                        Você não tem nenhuma mensagem no seu histórico.
                      </div>
                    </div>
                  </div>
                </div>

      <?php
      }
      
      $codigo_destinatario = $_GET['id'];

      $tipo = new Usuario();
      $tipo_destinatario = $tipo->busca_dados($codigo_destinatario);
      
      $verificacao = new Mensagem();
      $verificacao_tipo = $verificacao->verifica_usuario_mesmo_tipo($tipo_destinatario[0]['tipo_usuario']);

      if ($verificacao_tipo == true) {
        header('location:./?pgs=modal_mensagem_erro');
        exit();
      }
     
      $mensagens = new Mensagem();
      $todas_mensagens = $mensagens->busca_todas_mensagens($codigo_remetente, $codigo_destinatario);

      $dados_chat = new Profissional();
      $dados_chat_profissional = $dados_chat->busca_dados($codigo_destinatario);

                            

?>     
  <!--começo do chat/form -->   
  <div class="ui nine wide column ">
    <div class="ui segment">
      <div class="ui stackable column">

      <div class="ui items">
        <div class="item">
        <div class="ui tiny circular image"><img src="./imagens/<?= $dados_chat_profissional[0]["foto_perfil"];?>"></div>
        <div class="content">
          <div class="header"><?= $dados_chat_profissional[0]['nome_usuario'];?></div>
            <div class="meta"><?= $dados_chat_profissional[0]['area_atua'];?></div>
          </div>
        </div>
      </div>

      <div class="ui fitted divider"></div>

      <div class="chat-history">
<?php 
      foreach ($todas_mensagens as $mensagem){
                                     
        if ($mensagem['cod_usuario_destinatario'] == $codigo_destinatario) {
            
?>      



        <div class="clearfix">
        <div class="ui compact message gray float-right ">

          <div class="message-data align-right">
            <span class="message-data-time" ><?= $mensagem['data_mensagem'];?></span>
          </div>
          <div class="message other-message float-right">
            <?= $mensagem['texto_mensagem'];?>
          </div>
        </div>
        </div>




<?php 
        }elseif($mensagem['cod_usuario_remetente'] == $codigo_destinatario){ 
                     
?>        


        <div class="clearfix">
        <div class="ui compact message blue">

          <div class="message-data">
            <span class="message-data-time" ><?= $mensagem['data_mensagem'];?></span>
          </div>
          <div class="message">
            <?= $mensagem['texto_mensagem'];?>
          </div>
        </div>
        </div>

    
<?php   }  
      }
?> 


      </div><!--fim chat-history --> 


      <div class="ui fitted divider"></div>
      <p></p>
              <form class="ui form" method="POST" action="./Controllers/processa_mensagem.php">
                    <input type="hidden" name="codigo_usuario_remetente" value="<?=$codigo_remetente;?>">
                    <input type="hidden" name="codigo_usuario_destinatario" value="<?=$codigo_destinatario;?>">
                  <div class="ui fluid input action">
                    <input type="text" placeholder="Mensagem..." name="texto_mensagem">
                    <button type="submit" class="ui button" name="Enviar">Enviar</button>
                  </div>
                </div>
              </form>     
      </div><!--fim right floated column  --> 
    </div><!--fim ui segment --> 
  </div><!--fim nine wide column --> 

  <!--fim do chat/form --> 
<?php }else{
   header('location:./?pgs=mensagem');
  } ?>  
</div><!--fim da grid --> 

</body>
</html>