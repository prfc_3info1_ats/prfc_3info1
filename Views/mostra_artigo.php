<!DOCTYPE>
<html>
  <head>
    <meta charset="utf-8" />
    <link href="./semantic/semantic.css" rel="stylesheet">
    <meta content="IE=edge,chrome=1" http-equiv="X-UA-Compatible" />
    
    <title>Mostra Artigo</title>
    
    <link href="./css_js/MeuCSS.css" rel="stylesheet" type="text/css" />

 

  </head>

  <body class="template">

  <?php  
    include_once './Models/Artigo.php';
    include_once './Models/Profissional.php';
    include_once './Models/Login.php';

    $logado = new Login();
    $esta_logado=$logado->taLogado();
                
    if ($esta_logado == false) {
      header('location:./?pgs=inicial');
    }


    $id = $_GET['id'];
    $artigo = new Artigo();
    $mostra_artigo = $artigo->busca_artigo($id);
    $exibicao = $mostra_artigo[0];

    $busca_foto = new Artigo();
    $codigo_foto = $exibicao['cod_usuario_profissional'];
    $foto = $busca_foto->buscar_foto_profissional($codigo_foto);
    $foto_concatenar = $foto[0]['foto_perfil'];
    $foto_mostrada = "imagens/".$foto_concatenar;

    $dados_pro = new Profissional();
    $dados_profissional = $dados_pro->busca_dados($exibicao['cod_usuario_profissional']);
  ?>

    <div class="ui  text container">
    <div class="ui  main text container">
      <h2 class="ui header">
        <?=$exibicao['titulo_artigo'];?>
      </h2>
      <div class="ui label right">
        <?=$exibicao['tags'];?>
      </div>
    </div>
    <div class="ui borderless stackable menu">
      
        <div class="item imagem-div">
          <a href='?pgs=perfil_profissional&pg=1&id=<?=$exibicao["cod_usuario_profissional"];?>'>
            <img class="ui mini rounded image" src="<?=$foto_mostrada;?>"/>
          </a>
        </div>
          <a class="item" href='?pgs=perfil_profissional&pg=1&id=<?=$exibicao["cod_usuario_profissional"];?>'> <?= $dados_profissional[0]['nome_usuario']?></a>
          <a class="ui right floated item" href="?pgs=mais_artigos_profissional&pg=1&id=<?=$exibicao["cod_usuario_profissional"];?>">Mais Artigos</a>
          <div class="item">
          <?php 
            $avaliacao = new Artigo();
            $avaliacao_positiva = $avaliacao->busca_avaliacao_artigo(1, $id);
            $avaliacao_negativa = $avaliacao->busca_avaliacao_artigo(0, $id);
          ?>
            <form method="post" action="./Controllers/processa_avaliacao_artigo.php">
              <div class="ui buttons rounded">
                <button class="ui button" name="avaliacao" value="não"><i class="thumbs down icon"></i><?=$avaliacao_negativa;?></button>
                  <div class="or" data-text="ou"></div>
                    <button class="ui grey button" name="avaliacao" value="sim"><i class="thumbs up icon"></i><?=$avaliacao_positiva;?></button>
                    <input type="hidden" name="id" value=<?=@htmlspecialchars($_GET['id']);?>> 
              </div>
            </form>        
          </div>   
      
    </div>

    
    <div class="ui basic segment">
      <img class="ui medium left floated rounded image" data-src="square-image.png" src="./imagens/<?=$exibicao["imagem"];?>"/>
        <div class="texto_artigo">
          <?=$exibicao['texto_artigo'];?>
        </div>
    </div>

      <?php include 'comentario.php'; ?>

      </div>